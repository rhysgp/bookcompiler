scalaVersion := "2.12.1"

name := "RhysSoft Book Compiler"

version := "1.0"

resolvers += "Scalaz Bintray Repo" at "http://dl.bintray.com/scalaz/releases"

libraryDependencies ++= Seq(
	"com.google.api-client" % "google-api-client" % "1.20.0",
	"com.google.oauth-client" % "google-oauth-client-jetty" % "1.20.0",
	"com.google.apis" % "google-api-services-drive" % "v3-rev1-1.20.0",
	"com.typesafe.scala-logging" %% "scala-logging" % "3.5.0",
	"ch.qos.logback" % "logback-classic" % "1.1.7",
	"org.scala-lang.modules" %% "scala-xml" % "1.0.6",

	"org.specs2" %% "specs2-core" % "3.8.6" % "test"
)


