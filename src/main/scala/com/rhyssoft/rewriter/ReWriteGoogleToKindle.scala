package com.rhyssoft.rewriter

import java.nio.file.{Files, Path, Paths, StandardCopyOption}

import com.rhyssoft.IOUtils._
import com.rhyssoft.bookcompiler.model.{Author, Book, BookMetadata}
import com.rhyssoft.bookcompiler.transforms._
import com.rhyssoft.bookcompiler.{ChapterFile, GoogleDriveFilesRetriever, PathFilesRetriever}
import com.rhyssoft.kindlegen.KindleGen


/**
  * TODO - Need to fetch? and re-write the HTML so that the images are displayed!
  *        It looks like the URLs to the images work; but I need to copy them locally for kindlegen to work.
  *        This should be straightforward.
  *
  * TODO - Need to update the styles so that we have the styles we want when they're on the kindle.
  *
  * TODO - Would be nice to be able to detect speech so that I can re-write speech to use either single quotes or
  *        double quotes. Would also need to be able to detect speech within speech. Special note would need to be
  *        taken of paragraphs where the speech hasn't ended, but where continuing open speech marks are required.
  */
class ReWriteGoogleToKindle(bookTitle: String, author: Author, drivePath: Seq[String], coverImagePath: Path, googleHtmlPath: Path, outputPath: Path, downloadFresh: Boolean) {

  private val fileRetriever = if (downloadFresh) new GoogleDriveFilesRetriever(drivePath, googleHtmlPath) else new PathFilesRetriever(googleHtmlPath)
  private val outputTransform = new InternalToHtmlXmlTransform()
  private val tocTransform = new TocTransform(bookTitle, outputPath)
  private val opfTransform = new OpfTransform(bookTitle, "en", author, coverImagePath.toString, outputPath)
  private val googleHtml2Model = new GoogleHtmlToInternalModel(bookMetadata, fileRetriever, outputPath)

  def rewrite(): Book = {
    val book = googleHtml2Model.convert()
    write(book)
    createAdditionalAssets()
    KindleGen.run(outputPath)
    book
  }

  private def createAdditionalAssets(): Unit = {
    val chapterFiles = ChapterFile.listDir(outputPath).filter(_.filename.endsWith(".html"))
    tocTransform.process(chapterFiles)
    opfTransform.process(chapterFiles)
    Files.copy(Paths.get(s"frontpage/$coverImagePath"), outputPath.resolve(coverImagePath.getFileName), StandardCopyOption.REPLACE_EXISTING)
  }

  private def write(book: Book): Unit =  {
    Files.createDirectories(outputPath)
    val elems = book.chapters.map(outputTransform.process)

    elems.zip(book.chapters).zipWithIndex.map{ case ((elem, chap), i) =>
      val idx = "%02d".format(i + 1)
      val path = outputPath.resolve(s"$idx. ${chap.metadata.title}.html")
      autoClose(Files.newBufferedWriter(path))(_.write(elem.toString))
      println(s"File written to ${path.toString}")
      path
    }
  }

  private def bookMetadata: BookMetadata = BookMetadata(bookTitle, author, "Google Drive")

}

object ReWriteGoogleToInternalApp extends App {

  val downloadFresh = false
  val googleDriveFolderPath = Seq("Ash and the Egyptian Sarcophagus", "Draft 3")
  val googleHtmlPath = Paths.get("/Users/rhys/bookcompiler/google-html")
  val internalHtmlPath = Paths.get("/Users/rhys/bookcompiler/internal-html")
  val coverFilename = "Ash_egypt_front_page.jpg"
  val coverImagePath = Paths.get(coverFilename)

  val title = "Ash and the Egyptian Sarcophagus"
  val author = Author("Taliesin", "Wychwood")

  val rewriter = new ReWriteGoogleToKindle(title, author, googleDriveFolderPath, coverImagePath, googleHtmlPath, internalHtmlPath, downloadFresh = downloadFresh)

  val book = rewriter.rewrite()
}

