package com.rhyssoft.rewriter

import java.nio.file.{Path, Paths}

import com.rhyssoft.IOUtils
import com.rhyssoft.bookcompiler.model.{Author, BookMetadata}
import com.rhyssoft.bookcompiler.transforms.InternalToRhysSoftFormat
import com.rhyssoft.bookcompiler.{FileRetriever, PathFilesRetriever}

class RewriteGoogleHtmlToRhysSoftFormat(fileRetriever: FileRetriever) {

  def rewrite(bookMetadata: BookMetadata, destDir: Path): Seq[Path] = {
    val book = new GoogleHtmlToInternalModel(bookMetadata, fileRetriever, destDir).convert()

    val outFiles = book.chapters.map(ch => {
      val chapNum = "%02d".format(ch.metadata.chapterNumber)
      val filename = s"$chapNum ${ch.metadata.title}"
      val fileText = InternalToRhysSoftFormat.process(ch)
      val outFile = destDir.resolve(filename + ".rs.txt")
      IOUtils.writeToFile(fileText, outFile)
      outFile
    })

    // also write out the book metadata:
    val bookFile = destDir.resolve("book.rs.txt")
    IOUtils.writeToFile(
      s"{title:${bookMetadata.title}{author:${bookMetadata.author.toString}",
      bookFile
    )

    outFiles :+ bookFile
  }

}

object RewriteGoogleHtmlToRhysSoftFormatApp extends App {

  val fileRetriever = new PathFilesRetriever(Paths.get("/Users/rhys/bookcompiler/google-html/"))
  new RewriteGoogleHtmlToRhysSoftFormat(fileRetriever).rewrite(
    BookMetadata("", Author("Taliesin", "Wychwood"), ""),
    Paths.get("/Users/rhys/bookcompiler/rhyssoft-format/")
  )
}
