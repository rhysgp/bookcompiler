package com.rhyssoft.rewriter

import java.nio.file.Path

import com.rhyssoft.bookcompiler.model.{Book, BookMetadata, Chapter}
import com.rhyssoft.bookcompiler.transforms._
import com.rhyssoft.bookcompiler.{ChapterFile, FileRetriever}

class GoogleHtmlToInternalModel(bookMetadata: BookMetadata, fileRetriever: FileRetriever, outputPath: Path) {

  import GoogleHtmlToInternalModel._

  private val imgProcessor = new ImgProcessor(outputPath)

  def convert(): Book = {
    val retrievedFiles = fileRetriever.retrieveFiles()
    convertAllGoogleHtmlToInternal(retrievedFiles)
  }

  def convertAllGoogleHtmlToInternal(retrievedFiles: Seq[ChapterFile]): Book = {
    val chapters = retrievedFiles.zipWithIndex.map{case (rf, i) =>
      println(rf.filePath)
      val chapter = convertGoogleHtmlToInternal(rf, i + 1)
      imgProcessor.process(chapter)
    }
    Book(bookMetadata, chapters.toList)
  }
}

object GoogleHtmlToInternalModel {
  private val chapterNumAndTitleTransform = new ChapterNumberAndTitleTransform()
  private val coalesceAdjacentIndentedParagraphsTransform = new CoalesceAdjacentIndentedParagraphsTransform()
  private val interParaSeparatorsProcessor = new InterParaSeparatorsProcessor()

  def convertGoogleHtmlToInternal(chapterFile: ChapterFile, chapterNum: Int): Chapter = {
    var chapter = GoogleDocHtmlToInternalTransform.process(chapterFile.filePath)
    chapter = chapterNumAndTitleTransform.process(chapter)
    chapter = RationaliseTextStylesTransform.process(chapter)
    chapter = coalesceAdjacentIndentedParagraphsTransform.process(chapter)
    chapter = chapter.copy(paragraphs = chapter.paragraphs.map(FixupEmptyParagraphsInlineContentTransform.process))
    chapter = RemoveEmptyInlineTextStyleTransform.process(chapter)
    chapter = interParaSeparatorsProcessor.process(chapter)
    chapter = chapter.copy(paragraphs =  chapter.paragraphs.map(p => p.copy( inlineContent = CoalesceIdenticalInlineContentTransform.process(p.inlineContent))))
    chapter.copy(metadata = chapter.metadata.copy(title = chapterFile.title, chapterNumber = chapterNum))
  }
}