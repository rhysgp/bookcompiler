package com.rhyssoft

import java.io._
import java.net.URL
import java.nio.charset.{Charset, StandardCharsets}
import java.nio.file.{Files, Path, StandardCopyOption, StandardOpenOption}
import java.util.stream.Collectors

import scala.collection.JavaConverters._

object IOUtils {

  def autoClose[A <: AutoCloseable,B](closeable: A)(f: A => B): B = try { f(closeable) } finally { closeable.close() }

  def rmDir(dir: Path, failSilently: Boolean = true): Unit = {
    val exists = Files.exists(dir)
    val isDir = Files.isDirectory(dir)
    if (exists && isDir) {
      val paths = autoClose(Files.walk(dir))(fileStream => fileStream.collect(Collectors.toList[Path])).asScala.toList.reverse
      paths.foreach(Files.delete)
    } else {
      if (!exists && !failSilently) throw new IOException(s"'${dir.toString}' does not exist")
      if (!isDir && !failSilently) throw new IOException(s"'${dir.toString}' is not a directory!")
    }
  }

  def outputToFile[R](f: File)(func: (OutputStream) => R): R = {
    f.getParentFile.mkdirs()
    autoClose(new BufferedOutputStream(new FileOutputStream(f))){ os =>
      func(os)
    }
  }

  def writeToFile[R](f: File)(func: Writer => R): R = {
    f.getParentFile.mkdirs()
    autoClose(new OutputStreamWriter(new FileOutputStream(f))){ w =>
      func(w)
    }
  }

  def writeToFile(s: String, p: Path, charset: Charset = StandardCharsets.UTF_8): Unit = {
    autoClose(Files.newBufferedWriter(p, charset, StandardOpenOption.CREATE))(_.write(s))
  }

  def readCompleteTextFile(p: Path, charset: Charset = StandardCharsets.UTF_8): String = {
    new String(Files.readAllBytes(p), charset)
  }

  def copy(fromUrl: URL, toFile: Path) = autoClose(fromUrl.openStream()) {
    (is: InputStream) => {
      Files.createDirectories(toFile.getParent)
      Files.copy(is, toFile, StandardCopyOption.REPLACE_EXISTING)
    }
  }
}
