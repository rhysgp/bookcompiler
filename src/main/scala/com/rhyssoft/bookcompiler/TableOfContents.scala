package com.rhyssoft.bookcompiler

import java.nio.file.Path

import com.rhyssoft.IOUtils._

case class TableOfContents(title: String, chapters: Seq[ChapterFile]) {

  def toHtmlFile(path: Path): Unit = writeToFile(toHtml, path)

  def toNcxFile(path: Path): Unit = writeToFile(toNcx, path)

  private def generateTocHtml: xml.Elem =
    <html>
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>{title}</title>
      </head>
      <body>
        <h2>Table of contents</h2>
        {
        chapters.zipWithIndex.map{ case (c, i) => <p class="tocsection"><a href={c.filename}>{chapterRefText(i, c)}</a></p> }
        }
      </body>
    </html>

  private def generateTocNcx: xml.Elem =
    <ncx  xmlns="http://www.daisy.org/z3986/2005/ncx/" version="2005-1">
      <head>
      </head>
      <docTitle><text>{title}</text></docTitle>
      <navMap>
        {
        navPoint("toc", 1, "Table of contents", "toc.html") ::
          chapters.toList.zipWithIndex.map{case (c, i) => navPoint(s"chapter$i", i + 2, chapterRefText(i, c), c.filename)}
        }
      </navMap>
    </ncx>

  private def navPoint(id: String, order: Int, label: String, href: String) =
    <navPoint id={id} playOrder={order.toString}>
      <navLabel><text>{label}</text></navLabel>
      <content src={href} />
    </navPoint>

  private def chapterRefText(index: Int, chapter: ChapterFile) = s"${index + 1} ${chapter.title}"

  private def toHtml =
    s"""
       |<!DOCTYPE html>
       |$generateTocHtml
     """.stripMargin


  def toNcx =
    s"""
       |<?xml version="1.0"?>
       |<!DOCTYPE ncx PUBLIC "-//NISO//DTD ncx 2005-1//EN" "http://www.daisy.org/z3986/2005/ncx-2005-1.dtd">
       |$generateTocNcx
     """.stripMargin
}
