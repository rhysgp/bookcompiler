package com.rhyssoft.bookcompiler.transforms

import com.rhyssoft.bookcompiler.model._

object InternalToRhysSoftFormat extends ContentBuilder[Chapter,String] {

  private val fontSizeRegex = ".*?font-size:([^;]*).*".r

  override def process(chapter: Chapter): String = {
    val output = new StringBuilder()
    outputMetadata(chapter.metadata, output)
    chapter.paragraphs.foreach(outputPara(_, output))
    output.toString()
  }

  private[transforms] def outputMetadata(metadata: ChapterMetadata, output: StringBuilder): StringBuilder = {
    output.append(s"{chapter number:${metadata.chapterNumber} title:§${metadata.title}§ type:${metadata.chapterType}}\n")
  }

  private[transforms] def outputPara(paragraph: Paragraph, output: StringBuilder): StringBuilder = {
    val style = paragraph.style match {
      case ParagraphStyle.Normal => ""
      case ParagraphStyle.Indented => ":quote"
      case ParagraphStyle.Continuation => ":cont"
      case ParagraphStyle.Separator => ":sep"
    }

    paragraph.inlineContent.foldLeft(output.append(s"{p$style}"))((sb, ic) => {
      ic.contentType match {
        case ContentType.Text =>
          outputInlineContent(ic, sb)
        case ContentType.Image =>
          sb.append(s"{img:images/${ic.alt}}")
        case ContentType.NewLine =>
          sb.append(s"{br}")
      }
    })

    output.append("\n")
  }

  private[transforms] def outputInlineContent(ic: InlineContent, sb: StringBuilder): StringBuilder = {
    rhysSoftStyle(ic) match {
      case Some(style) =>
        sb.append(s"{ic:$style}${ic.content}{ic}")
      case None =>
        sb.append(ic.content)
    }
    sb
  }

  private def rhysSoftStyle(ic: InlineContent): Option[String] = {
    ic.cssStyle match {
      case null => None
      case "font-style:italic" => Some("italic")
      case "font-weight:bold" => Some("bold")
      case "font-weight:400" => Some("bold")
      case "font-weight:400;font-style:italic" => Some("bold-italic")
      case "vertical-align:super" => Some("super")
      case "" => None
      case s: String if s.contains("font-weight:400") => Some("bold")
      case s: String if s.startsWith("color:") => None
      case fontSizeRegex(fs) => Some(s"fs$fs")
      case s =>
        println(s"Can't handle inline content with style of '$s'")
        None
    }
  }

  private def p(style: String, s: String) = {
    s"{p${styleSign(style)}}$s\n"
  }

  private def styleSign(style: String) =
    if (style == null || style.isEmpty) "" else s":$style"
}
