package com.rhyssoft.bookcompiler.transforms

import com.rhyssoft.bookcompiler.model.{ContentType, Paragraph}

/**
  * Where there is only one InlineContent in a Paragraph and it's empty (except for spaces), just make it empty.
  */
object FixupEmptyParagraphsInlineContentTransform extends ContentBuilder[Paragraph,Paragraph] {

  override def process(para: Paragraph): Paragraph = {

    lazy val singleIc = para.inlineContent.length == 1
    lazy val ic = para.inlineContent.head
    lazy val isText = ic.contentType == ContentType.Text
    lazy val isEmpty = ic.content == null || ic.content.trim.isEmpty

    if (singleIc && isText && isEmpty) {
      para.copy(inlineContent = Nil)
    } else {
      para
    }
  }
}
