package com.rhyssoft.bookcompiler.transforms

import com.rhyssoft.bookcompiler.model._
import com.rhyssoft.style.StyleParser

object RationaliseTextStylesTransform extends ContentBuilder[Chapter,Chapter] {

  val styleClassMappings = Map(
    "padding-top:10pt;margin:0;color:#000000;padding-left:0;font-size:11pt;padding-bottom:10pt;font-family:&quot;Arial&quot;;line-height:1.15;orphans:2;widows:2;text-align:center;padding-right:0" -> "bunImages"
  )

  override def process(chapter: Chapter): Chapter = rationalise(chapter)

  private def rationalise(chapter: Chapter): Chapter = {

    val information = gatherSpanInformation(chapter)

    var paras = chapter.paragraphs.map(para => {
      para.copy(inlineContent =
        para.inlineContent.map{
          case ic if ic.contentType == ContentType.Text => rationaliseText(information, ic)
          case ic                                       => ic
        }
      )
    })

    val paragraphInformation = gatherParagraphInformation(chapter)

//    paras = paras.zipWithIndex.map{case (p, i)  => rationaliseParagraphStyles(paragraphInformation, p, paras, i)}

    paras = paras.foldLeft(Nil:List[Paragraph])((newParas, p) => {
      newParas :+ rationaliseParagraphStyles(paragraphInformation, p, newParas)
    })


    chapter.copy(paragraphs = paras)
  }

  private def rationaliseParagraphStyles(information: ParagraphInformation, p: Paragraph, prevParas: Seq[Paragraph]): Paragraph = {
    val styles = StyleParser.parse(p.cssStyle)
    var styleClasses = Set[String]()
    val indented = (information.defaultMarginLeft.isEmpty && styles.contains("margin-left")) ||
       (information.defaultMarginLeft.nonEmpty && styles.contains("margin-left") && styles("margin-left") != information.defaultMarginLeft.get)
    val firstLineIndented = (information.defaultTextIndent.isEmpty && styles.contains("text-indent")) ||
      (information.defaultTextIndent.nonEmpty && styles.getOrElse("text-indent", "") == information.defaultTextIndent.get)

    styleClasses = styleClassMappings.get(p.cssStyle) match {
      case Some(styleClass) => styleClasses + styleClass
      case None => styleClasses
    }

    if (styles.get("text-align").contains("center"))
      styleClasses = styleClasses + "centre"

    val mainP =
      if (indented)
        p.copy(style = ParagraphStyle.Indented)
      else if (!firstLineIndented && prevParas.nonEmpty && prevParas.last.style == ParagraphStyle.Indented)
        p.copy(style = ParagraphStyle.Continuation)
      else
        p

    mainP.copy(styleClass = mainP.styleClass ++ styleClasses)
  }

  private def rationaliseText(information: SpanInformation, inlineContent: InlineContent): InlineContent = {
    var ic = inlineContent
    val styles: Map[String, String] = StyleParser.parse(inlineContent.cssStyle)

    if (styles.contains("font-weight") && styles("font-weight") != "400" && styles("font-weight") != "normal") { // font-weight:400 is 'normal'
      ic = ic.copy(styleClass = ic.styleClass + "bold")
    }

    if (styles.get("font-style").contains("italic")) {
      ic = ic.copy(styleClass = ic.styleClass + "italic")
    }

    ic
  }

  private def gatherSpanInformation(chapter: Chapter): SpanInformation = {
    val texts = chapter.paragraphs.flatMap(_.inlineContent).filter(_.contentType == ContentType.Text)
    val defaultSpanColour = texts
      .map(ic => StyleParser.parse(ic.cssStyle).getOrElse("color", ""))
      .groupBy(s => s)
      .mapValues(_.length)
      .toList
      .sortBy(_._2)(Ordering[Int].reverse)
      .map(_._1)
      .headOption

    SpanInformation(defaultSpanColour)
  }

  private def gatherParagraphInformation(chapter: Chapter): ParagraphInformation = {
    val styles = chapter.paragraphs.map(p => StyleParser.parse(p.cssStyle))

    val defaultMarginLeftStyle = styles.map(_.getOrElse("margin-left", ""))
      .groupBy(s => s)
      .mapValues(_.length)
      .toList
      .sortBy(_._2)(Ordering[Int].reverse)
      .map(_._1)
      .headOption

    val defaultTextIndent = styles.map(_.getOrElse("text-indent", ""))
      .groupBy(s => s)
      .mapValues(_.length)
      .toList
      .sortBy(_._2)(Ordering[Int].reverse)
      .map(_._1)
      .headOption

    ParagraphInformation(defaultMarginLeftStyle, defaultTextIndent)
  }
}

case class SpanInformation(defaultSpanColour: Option[String])

case class ParagraphInformation(defaultMarginLeft: Option[String], defaultTextIndent: Option[String])
