package com.rhyssoft.bookcompiler.transforms

import java.nio.file.Path

import com.rhyssoft.IOUtils._
import com.rhyssoft.bookcompiler.ChapterFile
import com.rhyssoft.bookcompiler.model.Author
import com.rhyssoft.opf.{Opf, OpfPackage}

class OpfTransform(bookTitle: String, language: String, author: Author, coverFilename: String, outputDir: Path) extends ContentBuilder[Seq[ChapterFile],OpfPackage] {

  override def process(chapters: Seq[ChapterFile]): OpfPackage = {
    val opfPackage = Opf.createOpfPackage(bookTitle, language, author, coverFilename, chapters)
    writeToFile(opfPackage.toXml.toString(), outputDir.resolve("book.opf"))
    opfPackage
  }
}
