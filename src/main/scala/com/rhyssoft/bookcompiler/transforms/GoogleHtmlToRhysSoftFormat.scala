package com.rhyssoft.bookcompiler.transforms

import java.io.ByteArrayInputStream
import java.nio.charset.StandardCharsets
import java.nio.file.{Files, Paths}

import com.rhyssoft.bookcompiler.GoogleDriveFilesRetriever
import com.rhyssoft.rewriter.GoogleHtmlToInternalModel

object GoogleHtmlToRhysSoftFormat extends App {

  private val drivePath = Seq("Ash and the Egyptian Sarcophagus", "Draft 3")
  private val gOutputPath = Paths.get("/Users/rhys/bookcompiler/google-html")
  private val rOutputPath = Paths.get("/Users/rhys/bookcompiler/rhyssoft-format")
  private val retriever = new GoogleDriveFilesRetriever(drivePath, gOutputPath)

  private val chapters = retriever.retrieveFiles().zipWithIndex.map{ case(chapterFile, i) =>
    val chapter = GoogleHtmlToInternalModel.convertGoogleHtmlToInternal(chapterFile, i + 1)
    val rsChapter = InternalToRhysSoftFormat.process(chapter)
    (chapterFile, rsChapter)
  }

  chapters.zipWithIndex.foreach{ case ((chapterFile, rsFormat), i) =>
    val p = rOutputPath.resolve(s"${i + 1}. ${chapterFile.title}.rs.txt")
    val is = new ByteArrayInputStream(rsFormat.getBytes(StandardCharsets.UTF_8))
    Files.copy(is, p)
  }
}
