package com.rhyssoft.bookcompiler.transforms

import java.net.URL
import java.nio.file.{Files, Path, Paths}

import com.rhyssoft.bookcompiler.model.{Chapter, ContentType, InlineContent, Paragraph}
import com.rhyssoft.IOUtils._

import scala.util.Try

/**
 * Processes images with full URL src to download it and change the src to be relative.
 */
class ImgProcessor(outputPath: Path, includeFileProtocol: Boolean = false) extends ContentBuilder[Chapter, Chapter] {

  val imgDir = outputPath.resolve("images")
  Files.createDirectories(imgDir)

  private var count = 0

  override def process(chapter: Chapter): Chapter = {
    chapter.copy(paragraphs = chapter.paragraphs.map(processPara))
  }

  private def processPara(para: Paragraph): Paragraph = {
    para.copy(inlineContent = para.inlineContent.map(processInlineContent))
  }

  private def processInlineContent(ic: InlineContent): InlineContent = {
    ic match {
      case InlineContent(c, s, alt, ContentType.Image, _) if c.startsWith("http") || (includeFileProtocol && c.startsWith("file")) =>
        val imgPath = Paths.get(s"images/${count}_$alt")
        count = count + 1
        Try{copy(new URL(c), outputPath.resolve(imgPath))} recover { case t => /**/ }
        InlineContent.img(imgPath.toString, s)
      case _ => ic
    }
  }
}
