package com.rhyssoft.bookcompiler.transforms

import com.rhyssoft.bookcompiler.model._

class CoalesceAdjacentIndentedParagraphsTransform extends ContentBuilder[Chapter,Chapter] {

  override def process(chapter: Chapter): Chapter = {
    coalesceAdjacentIndentedParagraphs(chapter)
  }

  private def coalesceAdjacentIndentedParagraphs(chapter: Chapter) = {
    val paras = chapter.paragraphs.foldLeft(Nil:List[Paragraph])((ps, p) => {
      if (p.style == ParagraphStyle.Indented && ps.nonEmpty && ps.head.style == ParagraphStyle.Indented) {
        ps.head.copy(inlineContent = ps.head.inlineContent ::: (InlineContent.br :: p.inlineContent)) :: ps.tail
      } else {
        p :: ps
      }
    })

    chapter.copy(paragraphs = paras.reverse.map(removeEmptyStartAndEnd))
  }

  private def removeEmptyStartAndEnd(paragraph: Paragraph): Paragraph = {
    if (paragraph.style == ParagraphStyle.Indented) {
      var content = paragraph.inlineContent
      content = removeEmptyStart(content)
      content = removeEmptyStart(content.reverse).reverse
      paragraph.copy(inlineContent = content)
    } else {
      paragraph
    }
  }

  private def removeEmptyStart(content: List[InlineContent]): List[InlineContent] = {
    content match {
      case InlineContent(s, _, _, ContentType.Text, _) :: InlineContent.br :: rest if s.trim.isEmpty => rest
      case c => c
    }
  }
}
