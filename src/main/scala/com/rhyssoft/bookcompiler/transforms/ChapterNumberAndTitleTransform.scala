package com.rhyssoft.bookcompiler.transforms

import com.rhyssoft.bookcompiler.model.Chapter

/**
 * This transform simple removes the first two paragraphs.
 */
class ChapterNumberAndTitleTransform extends ContentBuilder[Chapter,Chapter] {

  override def process(ch: Chapter): Chapter = {
    ch.copy(paragraphs = ch.paragraphs.drop(2))
  }
}
