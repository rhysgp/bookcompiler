package com.rhyssoft.bookcompiler.transforms

import java.nio.file.{Files, Path}

import scala.util.Try

import com.rhyssoft.IOUtils._
import com.rhyssoft.bookcompiler.model.ParagraphStyle.ParagraphStyle
import com.rhyssoft.bookcompiler.model._

object RhysSoftFormatToInternalModel extends ContentBuilder[Path,Chapter] {

  private val paraTypeRegex = "\\s*\\{p:?([a-zA-Z0-9-]*)\\}(.*)".r

  private val chapterMetadataRegex = "\\s*\\{chapter(.*)\\}\\s*".r

  private val lineBreakRegex = "(.*?)\\{br\\}(.*)".r
  private val icRegex = "(.*?)(\\{ic:[a-z][a-z0-9]*\\}.*?\\{ic\\})(.*)".r
  private val imgRegex = "(.*?)(\\{img:[^}]+\\})(.*)".r
  private val imgInternalRegex = "\\{img:(.*?)(?::(.*?))?}".r

  private val metadataTitleRegex = ".*?title:§([^§]*)§.*".r
  private val metadataNumberRegex = ".*?number:(\\d{1,5}).*".r
  private val chapterTypeRegex = ".*?type:(Foreword|Preface|Chapter|Appendix).*".r
  private val icInternalsRegex = "^\\{ic:(.*?)\\}(.*?)\\{ic\\}$".r

  override def process(chapterFile: Path): Chapter = {
    autoClose(Files.lines(chapterFile))(process)
  }

  private[transforms] def process(stream: java.util.stream.Stream[String]): Chapter = {
    var chapterMetadata: Option[ChapterMetadata] = None
    var paragraphs = List[Paragraph]()
    stream.forEach {
      // NB IntelliJ complains, but this compiles fine!!
      case chapterMetadataRegex(metadata) =>
        chapterMetadata = Some(parseMetadata(metadata))
      case paraTypeRegex(pStyle, text) =>
        paragraphs = paragraphs :+ Paragraph(parseInlineContent(text), paragraphStyle(pStyle))
      case "" => /* no-op */
      case line: String if line != null && line.trim.startsWith("#") => // no-op
      case line: String =>
        Try {
          val lastPara = paragraphs.last
          val newLastPara = lastPara.copy(inlineContent = (lastPara.inlineContent :+ InlineContent.br) ::: parseInlineContent(line))
          paragraphs = paragraphs.take(paragraphs.length - 1) :+ newLastPara
        }.recover {
          case t =>
            throw new Exception("Error processing line " + line, t)
        }.get
    }
    Chapter(chapterMetadata.get, paragraphs)
  }

  private def parseMetadata(s: String): ChapterMetadata = {
    val title = s match {
      case metadataTitleRegex(t) => t
      case _ => throw new Exception("Missing title")
    }
    val chapterNumber = s match {
      case metadataNumberRegex(i) => i.toInt
      case _ => throw new Exception("Missing chapter number")
    }
    val chapterType = s match {
      case chapterTypeRegex(t) => ChapterType.withName(t)
      case _ => throw new Exception("Missing chapter type")
    }
    ChapterMetadata(title, chapterNumber, chapterType)
  }
  
  private def paragraphStyle(s: String): ParagraphStyle = {
    s match {
      case "quot" => ParagraphStyle.Indented
      case "quote" => ParagraphStyle.Indented
      case "sep" => ParagraphStyle.Separator
      case "separator" => ParagraphStyle.Separator
      case "cont" => ParagraphStyle.Continuation
      case "continuation" => ParagraphStyle.Continuation
      case _ => ParagraphStyle.Normal
    }
  }


  // NB This is recursive! Could be problematic for long paragraphs with lots of styles!
  private def parseInlineContent(s: String): List[InlineContent] = {
    s match {
      case lineBreakRegex(pre, post) =>
        parseInlineContent(pre) ::: InlineContent.br :: parseInlineContent(post)
      case imgRegex(pre, ic, post) =>
        parseInlineContent(pre) ::: parseIcImg(ic) :: parseInlineContent(post)
      case icRegex(pre, ic, post) =>
        parseInlineContent(pre) ::: parseIcText(ic) ::: parseInlineContent(post)
      case _ =>
        parseIcText(s)
    }
  }

  private[transforms] def parseIcImg(s: String): InlineContent = {
    s match {
      case imgInternalRegex(img, style) =>
        InlineContent.img(content = img, style = Option(style).getOrElse(""))
      case _ => throw new Exception(s"Can't parse as img: '$s'")
    }
  }

  private def parseIcText(text: String): List[InlineContent] = {
    text match {
      case null                       => Nil
      case ""                         => Nil
      case s if s.trim.isEmpty        => Nil
      case icInternalsRegex(style, s) => List(InlineContent.txt(s, style))
      case s                          => List(InlineContent.txt(s))
    }
  }
}