package com.rhyssoft.bookcompiler.transforms

import com.rhyssoft.bookcompiler.model._

class InternalToHtmlXmlTransform extends ContentBuilder[Chapter,xml.Elem] {

  override def process(chapter: Chapter): xml.Elem = {

    val title = if (chapter.metadata.title.isEmpty) "\u00a0" else chapter.metadata.title

    <html>
      <head>
        <meta http-equiv="Content-Type" content="text/html;charset=utf8"/>
        <title>{chapter.metadata.title}</title>
        <style type="text/css">{"""
            body               { font-family: Arial; font-size: 1 em }

            div.chapterNumber  { font-size: 3em; text-align: center }
            div.chapterTitle   { font-size: 2em; text-align: center; margin-bottom: 10px; }

            p                  { margin: 0 0 0 0; padding: 0 0 0 0; }
            p.continuation     {}
            p.normal           { text-indent: 25pt; }
            p.indented         { margin: 10px 0 10px 35pt; }
            p.separator        { text-indent: 0; text-align: center; margin: 40px 0 40px 25%; width: 50%; height: 0; border: 1px solid black; border-top: 0;  }
            p.centre           { text-align: center }
            p.bunImages        { padding: 10pt 0 10pt 0; margin: 0; line-height: 1.15; orphans: 2; widows: 2; text-align: center }
            p.bunImages.normal { text-indent: 0 }

            span.italic        { font-style:italic }
            span.bold          { font-weight:bold }
        """}</style>
      </head>
      <body>
        <div class="chapterNumber">Chapter {chapter.metadata.chapterNumber}</div>
        <div class="chapterTitle">{title}</div>
        {chapter.paragraphs.map(convertPara)}
      </body>
    </html>
  }

  private[transforms] def convertPara(para: Paragraph): xml.Elem = {
    val mainParaClassName = para.style match {
      case ParagraphStyle.Indented     => "indented"
      case ParagraphStyle.Continuation => "continuation"
      case _                           => "normal"
    }

    val pClassName = (para.styleClass + mainParaClassName).mkString(" ")

    if (para.style == ParagraphStyle.Separator ) {
      <p class="separator"></p>
    } else {
      <p class={pClassName}>{
        para.inlineContent.map {
          case InlineContent(content, style, _, ContentType.Text, styleClass) =>
            <span class={styleClass.mkString(" ")}>{content}</span>
          case InlineContent(content, style, _, ContentType.Image, styleClass) =>
            <img src={content} class={styleClass.mkString(" ")} />
          case InlineContent(_, _, _, ContentType.NewLine, _) =>
            <br/>
          case _ => throw new Exception("not expecting this!!!")
        }
        }</p>
    }
  }
}

