package com.rhyssoft.bookcompiler.transforms

import com.rhyssoft.bookcompiler.model.{Chapter, ContentType, InlineContent}

object RemoveEmptyInlineTextStyleTransform extends ContentBuilder[Chapter, Chapter] {

  override def process(chapter: Chapter): Chapter = {
    chapter.copy(paragraphs = chapter.paragraphs.map(p => p.copy(inlineContent = processInlineContent(p.inlineContent))))
  }

  private def processInlineContent(inlineContent: List[InlineContent]): List[InlineContent] = {

    inlineContent.filter(ic => ic.contentType != ContentType.Text || !ic.content.isEmpty)

  }

}
