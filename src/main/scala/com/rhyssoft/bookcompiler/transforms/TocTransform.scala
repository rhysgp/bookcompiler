package com.rhyssoft.bookcompiler.transforms

import java.nio.file.Path

import com.rhyssoft.bookcompiler.{ChapterFile, TableOfContents}

/**
 * Generate the TOC from the chapter files.
 */
class TocTransform(bookTitle: String, outputDir: Path) extends ContentBuilder[Seq[ChapterFile], TableOfContents] {

  override def process(chapterFiles: Seq[ChapterFile]): TableOfContents = {
    val toc = TableOfContents(bookTitle, chapterFiles)

    toc.toHtmlFile(outputDir.resolve("toc.html"))
    toc.toNcxFile(outputDir.resolve("toc.ncx"))

    toc
  }
}
