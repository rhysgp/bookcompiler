package com.rhyssoft.bookcompiler.transforms

/**
 * Convert content from one type to another. E.g. from Google Doc export to internal format.
 */
trait ContentBuilder[T,U] {
  def process(source: T): U
}
