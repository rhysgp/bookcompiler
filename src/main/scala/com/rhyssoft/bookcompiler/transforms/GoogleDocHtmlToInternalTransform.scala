package com.rhyssoft.bookcompiler.transforms

import java.io.Reader
import java.nio.charset.StandardCharsets
import java.nio.file.{Files, Path}

import com.rhyssoft.IOUtils._
import com.rhyssoft.bookcompiler.TreeParser
import com.rhyssoft.bookcompiler.model._
import com.rhyssoft.htmlparser.{Element, Node, Text}

/**
 * Converts from Google Doc HTML format to internal. Doesn't do the download, but converts from already-downloaded files.
 */
object GoogleDocHtmlToInternalTransform extends ContentBuilder[Path,Chapter] {

  override def process(source: Path): Chapter = {
    // Parse the HTML to get a List[Node]
    autoClose(Files.newBufferedReader(source, StandardCharsets.UTF_8))(process)
  }

  def process(reader: Reader): Chapter = {
    val nodes = TreeParser.parseText(reader, onlyWithinBody = true)
    transform(nodes)
  }

  def transform(nodes: List[Node]): Chapter = {
    val paraNodes = groupIntoParas(nodes)
    Chapter(paraNodes.map(convertToPara))
  }

  private[transforms] def groupIntoParas(nodes: List[Node]): List[List[Node]] = {
    val paras = nodes.foldLeft(Nil:List[List[Node]])((nodeGroups, node) => {
      node match {
        case e: Element if (e.name == "p" || e.name.matches("h[1-9]") ) && e.isStartElement =>
          List(e) :: nodeGroups
        case n =>
          val ng = n :: nodeGroups.head
          if (nodeGroups.isEmpty)
            ng :: Nil
          else
            ng :: nodeGroups.tail
      }
    })

    paras.map(_.reverse).reverse
  }

  private[transforms] def convertToPara(nodes: List[Node]): Paragraph = {
    var cssStyle = ""
    val content = nodes.foldLeft(Nil: List[InlineContent])((contentList, node) => {
      node match {

        case e: Element if e.isStartElement && (e.name == "p" || e.name.matches("h[1-9]")) && contentList.isEmpty =>
          cssStyle = e.attributes.getOrElse("style", "")
          contentList // no-op, but don't emit warning

        case e: Element if e.isStartElement && e.name == "span" =>
          InlineContent.txt(style = e.attributes.getOrElse("style", "")) :: contentList

        case e: Element if e.name == "br" =>
          InlineContent.br :: contentList

        case e: Element if e.isStartElement && e.name == "img" =>
          val src = e.attributes("src")
          val style = e.attributes.getOrElse("style", "")
          val alt = e.attributes.getOrElse("alt", "")
          InlineContent.img(src, style, alt) :: contentList

        case t: Text if contentList.nonEmpty =>
          val lastInlineContent = contentList.head

          if (lastInlineContent.contentType == ContentType.Text) {
            lastInlineContent.copy(content = lastInlineContent.content + t.text) :: contentList.tail
          } else {
            InlineContent.txt(content = t.text) :: contentList
          }

        case e: Element if e.isEndElement  =>
          // do nothing
          contentList

        case n =>
          println(s"GoogleDocHtmlToInternalTransform.convertToPara(): ignoring unexpected node: $n" )
          contentList
      }
    })

    Paragraph(cssStyle = cssStyle, content.reverse)
  }
}
