package com.rhyssoft.bookcompiler.transforms

import com.rhyssoft.bookcompiler.model._

/**
 * Replaces asterisked breaks with special 'Inter-paragraph' paragraph.
 */
class InterParaSeparatorsProcessor extends ContentBuilder[Chapter, Chapter] {
  override def process(chapter: Chapter): Chapter = {
    val paragraphs = chapter.paragraphs.foldLeft(Nil:List[Paragraph])((paras, p) => {
      p :: paras.take(2) match {

        case Paragraph(_, Nil, ParagraphStyle.Normal, _) :: Paragraph(_, ic :: Nil, ParagraphStyle.Normal, _) :: Paragraph(_, Nil, ParagraphStyle.Normal, _) :: Nil if ic.content.filter(_ == '*') == "***"  =>
          Paragraph.separator :: paras.drop(2)

        case Paragraph.empty :: Paragraph(_, ic :: Nil, ParagraphStyle.Normal, _) :: Paragraph.empty :: Nil if ic.content.filter(_ == '*') == "***"  =>
          Paragraph.separator :: paras.drop(2)

        case _ =>
          p :: paras
      }
    })
    chapter.copy(paragraphs = paragraphs.reverse)
  }
}
