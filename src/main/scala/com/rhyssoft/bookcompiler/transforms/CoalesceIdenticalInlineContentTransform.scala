package com.rhyssoft.bookcompiler.transforms

import com.rhyssoft.bookcompiler.model.{ContentType, InlineContent}

/**
  * Where there are two InlineContent objects with identical styles and types, merge them.
  */
object CoalesceIdenticalInlineContentTransform extends ContentBuilder[List[InlineContent],List[InlineContent]] {

  override def process(source: List[InlineContent]): List[InlineContent] = {
    source.foldLeft(List[InlineContent]())((coalesced, ic) => {
      val lastInContOpt = coalesced.lastOption
      if (lastInContOpt.nonEmpty &&
            lastInContOpt.get.contentType == ContentType.Text &&
            ic.contentType == ContentType.Text &&
            lastInContOpt.get.cssStyle == ic.cssStyle) {
        coalesced.take(coalesced.length - 1) :+ coalesced.last.copy(content = coalesced.last.content + ic.content)
      } else {
        coalesced :+ ic
      }
    })
  }

}
