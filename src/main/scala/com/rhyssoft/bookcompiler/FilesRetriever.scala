package com.rhyssoft.bookcompiler

import java.nio.file.{Files, Path}
import java.util.stream.Collectors

import com.rhyssoft.IOUtils._
import com.rhyssoft.googledrive.DocumentExport

import scala.collection.JavaConverters._

trait FileRetriever {
  def retrieveFiles(): Seq[ChapterFile]
}

class GoogleDriveFilesRetriever(driveDirectoryPath: Seq[String], downloadDirectory: Path) extends FileRetriever {

  override def retrieveFiles(): Seq[ChapterFile] = {
    Files.createDirectories(downloadDirectory)
    val files = DocumentExport.export(driveDirectoryPath, downloadDirectory)

    println("Downloaded the following files: ")
    files.foreach(f => println(s"\t${f.title}\n\t\t${f.file.getAbsolutePath}"))

    files.map( df => ChapterFile(df.file.toPath) )
  }
}

class PathFilesRetriever(dir: Path) extends FileRetriever  {
  def retrieveFiles(): Seq[ChapterFile] = {
    val files = autoClose(Files.list(dir))(_.collect(Collectors.toList[Path]))
      .asScala
      .toList
      .filterNot(_.getFileName.toString.startsWith("."))

    files.foreach(f => println(f.toString))

    files.map(ChapterFile(_))
  }
}
