package com.rhyssoft.bookcompiler

import java.io.InputStream
import java.nio.file.{Files, Path}
import java.util.stream.Collectors

import com.rhyssoft.IOUtils._

import scala.collection.JavaConverters._

case class ChapterFile(title: String, filePath: Path) {

  val filename = filePath.getFileName.toString

  def withContentStream[T](f: InputStream => T): T = {
    autoClose(Files.newInputStream(filePath))(f)
  }
}

object ChapterFile {
  private val titleRegex = "\\d+\\.\\s*(.*?)(\\s*\\.html)?".r

  def apply(filePath: Path): ChapterFile = {
    val title = filePath.getFileName.toString match {
      case titleRegex(t, _) => t.trim
      case t => t
    }
    ChapterFile(title, filePath)
  }

  def listDir(chaptersDir: Path): Seq[ChapterFile] = {
    autoClose(Files.list(chaptersDir))(pathStream => pathStream.collect(Collectors.toList[Path]))
      .asScala
      .filter(Files.isRegularFile(_))
      .map(ChapterFile(_))
      .toList
  }
}
