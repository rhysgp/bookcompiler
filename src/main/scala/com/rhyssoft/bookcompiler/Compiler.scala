package com.rhyssoft.bookcompiler

import java.io._
import java.net.URL
import java.nio.charset.StandardCharsets
import java.nio.file.{Files, Paths, StandardCopyOption}

import com.rhyssoft.{AutoDelete, IOUtils}
import com.rhyssoft.IOUtils._
import com.rhyssoft.bookcompiler.model.Author
import com.rhyssoft.htmlparser.ElementType.ElementType
import com.rhyssoft.htmlparser.{Element, ElementType, Node, Text}
import com.rhyssoft.kindlegen.KindleGen
import com.rhyssoft.opf._
import com.rhyssoft.style.StyleParser
import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory

class Compiler(title: String, author: Author, inputDirectory: String, outputDirectory: String) {

  val logger = Logger(LoggerFactory.getLogger("com.rhyssoft.bookcompiler.Compiler"))

  def compile(drivePath: Seq[String]): Unit = {
//    val fileRetriever = new GoogleDriveFilesRetriever(drivePath, Paths.get(inputDirectory))
    val fileRetriever = new PathFilesRetriever(Paths.get(inputDirectory))

    val chapters = fileRetriever.retrieveFiles()
    val chapterContents = chapters.map(rewriteContents)
    val tocFile = new File(outputDirectory, "toc.html")
    val ncxTocFile = new File(outputDirectory, "toc.ncx")
    val bookOpf = new File(outputDirectory, "book.opf")
    val coverFilename = "Ash_egypt_front_page.jpg"
    val outputCoverPath = Paths.get(outputDirectory, coverFilename)

    val reWrittenChapters = outputContents(chapterContents)
//    outputImages(htmlFiles.filter{case (name,data) => name.endsWith(".png")})

    val toc = TableOfContents(title, chapters)
    toc.toHtmlFile(tocFile.toPath)
    toc.toNcxFile(ncxTocFile.toPath)

    outputPackage(Opf.createOpfPackage(title, "en", author, coverFilename, chapters), bookOpf)

    Files.copy(Paths.get(s"frontpage/$coverFilename"), outputCoverPath, StandardCopyOption.REPLACE_EXISTING)

    KindleGen.run(outputDirectory)

    // clean up output contents:
//    chapters.map(chapter => Paths.get(inputDirectory, chapter.filename)).foreach(AutoDelete.add)
    AutoDelete.add(tocFile.toPath)
    AutoDelete.add(ncxTocFile.toPath)
    AutoDelete.add(bookOpf.toPath)
    reWrittenChapters.foreach(f => AutoDelete.add(f.toPath))
    AutoDelete.add(outputCoverPath)

    logger.info(s"Files written to $outputDirectory")
  }

  private def outputImages(images: Map[String,Array[Byte]]): Unit = {
    images.foreach{case (filename, data) => outputToFile(new File(outputDirectory, filename))(_.write(data))}
  }

  private def output(nodes: List[Node], filename: String): File = {
    val file = new File(outputDirectory, filename)
    writeToFile(file) { writer =>
      nodes.foreach {
        case e: Element if e.isEndElement =>
          writer.write(e.toString)

          /*
           * Put a newline character after block elements, but not inline elements
           */
          figureElementType(e) match {
            case ElementType.Block => writer.write('\n')
            case ElementType.Inline => /* no-op */
          }

          writer.write("\n")
        case n: Node =>
          writer.write(n.toString)
      }
    }

    file
  }


  private def output(filename: String)(func: (OutputStream) => Unit): Unit = {

    val f = new File(outputDirectory, filename)

    f.getParentFile.mkdirs()

    val os = new BufferedOutputStream(new FileOutputStream(f))

    try {
      func(os)
    } finally {
      os.close()
    }

//    logger.info("Written to " + f.getAbsolutePath)
  }

  /**
   * Another pass to remove any CSS styles that are empty and remove them from any elements.
   */
  private def removeEmptyStyleClasses(nodes: List[Node], styles: Map[String, String]): List[Node] = {

    val emptyClasses = styles.filter{
      case (k,v) =>
        k.trim == ""
    }.values

    nodes.map{
      case e: Element =>
        if (e.attributes.contains("class") && emptyClasses.exists(_ == e.attributes("class"))) {
          e.copy(attributes = e.attributes - "class")
        } else e

      case n => n
    }
  }

  private def outputContents(contents: Seq[(ChapterFile,List[Node])]) =
    contents map { case (chapter, content) => output(content, chapter.filename) }


  private def outputPackage(pkg: OpfPackage, file: File): Unit =
    writeToFile(file){ writer =>
      writer.write(pkg.toXml.toString)
    }

  private def rewriteContents(chapter: ChapterFile) = {
    var nodes = List[Node]()
    var first = true
    var endNodes = List[Node]()

    val documentNodes = chapter.withContentStream(is =>
      TreeParser.parseText(new InputStreamReader(is, StandardCharsets.UTF_8), onlyWithinBody = !first)
    )

    if (!first) {
      nodes = Element("mbp:pagebreak", "", isEndElement = false, empty = true) :: nodes
    }

    documentNodes.foreach{
      case e: Element if e.isEndElement && (e.name == "body" || e.name == "html") =>
        endNodes = e :: endNodes

      case n: Node =>
        nodes = n :: nodes
    }

    first = false

    nodes = endNodes ::: nodes
    nodes = nodes.reverse
    nodes = removeNotes(nodes)

    val styles = collectStyles(nodes)

    nodes = rewriteStyles(nodes, styles)

    val fixedStyles = findAndReplaceNormalParagraphStyle(nodes, styles)

    nodes = insertStyleElement(nodes, fixedStyles)
    nodes = removeEmptyStyleClasses(nodes, fixedStyles)
    nodes = collapseEmptySpans(nodes)
    nodes = fixupImages(chapter, nodes)

    (chapter, nodes)
  }

  private def fixupImages(chapter: ChapterFile, nodes: List[Node]): List[Node] = {

    var writtenUrls = List[String]()

    nodes map {
      case e: Element if e.name == "img" =>
        val srcUrl = e.attributes("src")
        val filename =  chapter.filename + "_/" + e.attributes("alt")

        if (!writtenUrls.contains(srcUrl)) {
          println(s"""Found image: "$srcUrl". Copying to "$filename"""")
          val toFile = Paths.get(outputDirectory, filename)
          IOUtils.copy(new URL(srcUrl), toFile)
          AutoDelete.add(toFile.getParent)
          AutoDelete.add(toFile)
          writtenUrls = srcUrl :: writtenUrls
        }

        e.copy(attributes = e.attributes + ("src" -> filename))

      case n => n
    }
  }

  private def findAndReplaceNormalParagraphStyle(nodes: List[Node], styles: Map[String,String]): Map[String,String] = {

    var counts = Map[String, Int]()

    def add(style: String): Unit = {
      if (style.startsWith("block")) {
        counts = counts.filter{ case (k,v) => k != style } + (style -> (counts.getOrElse(style, 0) + 1) )
      }
    }

    nodes.foreach {
      case e: Element =>
        e.attributes.get("class").foreach(add)

      case _ => // do nothing
    }

    val biggestUsage = counts.foldLeft("")((lastStyleClass, t) => {

      val c = counts.getOrElse(lastStyleClass, 0)

      if (t._2 > c) {
        t._1
      } else {
        lastStyleClass
      }
    })

    // replace the biggest usage with standard styles:
    styles.filterNot{case (k,v) => v == biggestUsage} + ("margin:0;orphans:2;direction:ltr;widows:2;padding:0;text-indent:2em" -> biggestUsage)
  }

  private def collectStyles(nodes: List[Node]) = {

    var styles = Map[String,String]()

    var styleIndex = 0

    nodes.foreach{

      case e: Element =>
        if (e.isStartElement) {

          // does it have a style?
          e.attributes.get("style").foreach{ style =>

            val elementType = figureElementType(e)

            val trimmedStyle = trimStyle(style, elementType)

            if (!styles.contains(trimmedStyle)) {
              styles = styles + (trimmedStyle -> s"${elementType match {case ElementType.Block => "block"; case ElementType.Inline => "inline"}}_$styleIndex")
              styleIndex = styleIndex + 1
            }
          }
        }
      //        logger.info(s"element: ${e.name} ${if (e.isEndElement) " />" else ""}")
      case t: Text =>
      //        logger.info(s"text: ${t.text}")

    }

    styles
  }


  private def removeUnneededBlockStyles(map: Map[String, String]): Map[String, String] =
    map.filter {
      case (k,v) =>
        if (k == "font-family" ||
            k == "color" ||
            k == "max-width" ||
            k == "background-color" ||
            (k == "font-size" && v == "11pt"))
          false
        else
          true
    }

 private def replaceBlockStyles(map: Map[String, String]): Map[String, String] = {

    var _map = map

    if (_map contains "text-indent") {
      _map = _map + ("text-indent" -> "2em")
    }

    if (_map contains "font-size") {
      val fontSize = _map("font-size")

      if (fontSize.endsWith("pt") || fontSize.endsWith("px")) {
        val value = parseValue(fontSize)

        /*
         * This only really works if we know what the base px or pt value is. We'll incorrectly
         * treat px the same as pt and assume a base font-size of 11pt.
         */
        val pcValue = Math.round((value / 11.0) * 100.0)

        _map = _map + ("font-size" -> s"$pcValue%")
      }
    }

    _map
  }

  private def parseValue(fontSize: String): Int =
    fontSize.filter(c => Character.isDigit(c)).toInt

  private def removeUnneededInlineStyles(map: Map[String, String]): Map[String, String] = {
    map.filter{
      case (k,v) =>
        if (k == "color" ||
           (k == "vertical-align" && v == "baseline") ||
           (k == "font-size" && v == "11pt") ||
           (k == "font-style" && v == "normal") ||
           (k == "font-family") ||
           (k == "background-color") ||
           (k == "text-decoration" && v == "none") ||
           (k == "font-weight" && v == "normal"))
          false
        else
          true
    }
  }

  private def trimStyle(style: String, elementType: ElementType) = {

    var styles = StyleParser.parse(style)

    elementType match {
      case ElementType.Inline =>
        styles = removeUnneededInlineStyles(styles)

      case ElementType.Block =>
        styles = replaceBlockStyles(removeUnneededBlockStyles(styles))
    }

    val sb = new StringBuilder()

    styles.foreach{
      case (k,v) =>
        if (sb.nonEmpty) {
          sb.append(';')
        }
        sb.append(k).append(':').append(v)
    }

    sb.toString()
  }

  private def figureElementType(e: Element): ElementType =
    e.name match {
      case "body"  => ElementType.Block
      case "div"   => ElementType.Block
      case "p"     => ElementType.Block
      case "table" => ElementType.Block
      case "h1"    => ElementType.Block
      case "h2"    => ElementType.Block
      case "h3"    => ElementType.Block
      case "ul"    => ElementType.Block
      case "img"   => ElementType.Block
      case  _      => ElementType.Inline
    }

  private def rewriteStyles(nodes: List[Node], styles: Map[String,String]): List[Node] = {

    // re-write the elements that have styles as elements to have classes

    nodes.map {
      case e: Element =>
        val attr = e.attributes
        attr.get("style").map( style =>
          e.copy(attributes = replaceStyle(attr, styles, figureElementType(e)))
        ).getOrElse(e)

      case n => n
    }
  }

  private def replaceStyle(attributes: Map[String,String],
                           styles: Map[String,String],
                           elementType: ElementType): Map[String,String] = {

    var newAttributes: Map[String,String] = attributes

    attributes.get("style").foreach{ style =>
      newAttributes = attributes - "style"

      val className = styles(trimStyle(style, elementType))

      newAttributes = newAttributes + ("class" -> className)
    }

    newAttributes
  }

  private def insertStyleElement(nodes: List[Node], styles: Map[String,String]): List[Node] = {

    var foundHeadStart1 = false
    var foundHeadStart2 = false

    nodes.filter{ node =>

      var retVal = true

      if (foundHeadStart1) {
        retVal = false
      }

      node match {
        case e: Element if e.name == "head" =>
          foundHeadStart1 = true
        case _ => /* no-op */
      }

      retVal

    } ::: (Element("style", "type=\"text/css\"", isEndElement = false, empty = false) :: cssText(styles) :: Element("style", "", isEndElement = true, empty = false) :: Nil) ::: nodes.filter{
      node =>
        var retVal = false

        if (foundHeadStart2) {
          retVal = true
        }

        node match {
          case e: Element if e.name == "head" =>
            foundHeadStart2 = true
          case _ => /* no-op */
        }

        retVal
    }
  }

  private def cssText(styles: Map[String,String]): Text = {

    val sb = new StringBuilder

    styles.foreach{ case (key,value) =>
      sb.append(
        s"""
          |.$value {
          |   $key
          |}
        """.stripMargin)
    }

    Text(sb.toString())
  }

  private def removeNotes(nodes: List[Node]) = {

    var div = false
    var a = false

    nodes.filter{
      case e: Element if e.name == "div" =>
        div = e.isStartElement
        false

      case e: Element if e.name == "a" =>
        a = e.isStartElement
        false

      case n: Node =>
        !div && !a
    }
  }

  /**
   * Remove spans with no class.
   */
  private def collapseEmptySpans(nodes: List[Node]): List[Node] = {
    var foundEmptySpan = false
    nodes.filter {
      case e: Element if e.name == "span" && e.isStartElement && e.attributes.isEmpty =>
        foundEmptySpan = true
        false
      case e: Element if e.name == "span" && e.isEndElement && foundEmptySpan =>
        foundEmptySpan = false
        false
      case _ => true
    }
  }
}

object CompilerApp extends App {

  val logger = Logger(LoggerFactory.getLogger("com.rhyssoft.bookcompiler.CompilerApp"))

  checkParams()

  new Compiler(
    "Ash and the Egyptian Sarcophagus",
    Author("Taliesin", "Wychwood"),
    args(0),
    args(1)
  ).compile(Seq("Ash and the Egyptian Sarcophagus", "Draft 3"))

  private def checkParams(): Unit = {
    if (args.length != 2) {
      outputUsage()
      sys.exit(-1)
    }

    val rawFolder = new File(args(0))
    if (!rawFolder.isDirectory) {
      logger.info(s"'${rawFolder.getAbsolutePath}' doesn't exist or isn't a folder.")
      outputUsage()
      sys.exit(-2)
    }

    val outputDir = new File(args(1))
    if (outputDir.exists() && !outputDir.isDirectory) {
      logger.info(s"'${outputDir.getAbsolutePath}' isn't a directory.")
      outputUsage()
      sys.exit(-3)
    }

    if (!outputDir.exists()) {
      logger.info(s"Creating directory '${outputDir.getAbsolutePath}'")
      if (!outputDir.mkdirs()) {
        logger.info(s"Failed to create directory structure to '${outputDir.getAbsolutePath}'")
        sys.exit(-4)
      }
    }
  }

  private def outputUsage(): Unit = {
    logger.info("Usage:\n  scala com.rhyssoft.bookcompiler.Compiler <contents-folder-name> <output-dir>")
  }
}
