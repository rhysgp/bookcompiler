package com.rhyssoft.bookcompiler.model

import com.rhyssoft.bookcompiler.model.ContentType.ContentType

case class InlineContent(
  content: String, // Should be either text or base64-encoded image data
  cssStyle: String, // e.g. bold, italic, font, font-size. Same as HTML/CSS
  alt: String,
  contentType: ContentType,
  styleClass: Set[String] = Set() // the actual class styles that we'll output
)

object InlineContent {
  def txt(content: String = "", style: String = "") = InlineContent(content, style, "", ContentType.Text)
  def img(content: String = "", style: String = "", alt: String = "") = InlineContent(content, style, alt, ContentType.Image)
  val br = InlineContent("", "", "", ContentType.NewLine)
}

