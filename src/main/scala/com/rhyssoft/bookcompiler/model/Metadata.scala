package com.rhyssoft.bookcompiler.model

import com.rhyssoft.bookcompiler.model.ChapterType.ChapterType

case class BookMetadata(
  title: String,
  author: Author,
  source: String // e.g. Google Drive location
)

case class ChapterMetadata(
  title: String,
  chapterNumber: Int,
  chapterType: ChapterType
)

case class Author(firstnames: Seq[String], lastname: String) {

  override def toString: String = firstnames.mkString("", " ", " ") + lastname
  def fileAs: String = lastname + firstnames.mkString(", ", " ", "")

  def toRhysSoftFormat: String = {
    s"""lastName:"$lastname";firstNames:"${firstnames.mkString(",")}""""
  }

}

object Author {
  def apply(firstname: String, lastname: String): Author = Author(List(firstname), lastname)

  private val regex = "lastName:\"([A-Za-z\\s-]*)\";firstNames:(.*?)\"".r
  def fromRhysSoftFormat(s: String): Author = {
    s match {
      case regex(lastName, firstNames) =>
        Author(
          firstNames.split(",").toSeq.map(_.trim).filter(_.nonEmpty),
          lastName
        )
      case _ =>
        throw new Exception(s"Invalid format for RhysSoft Author: $s")
    }
  }

}

object ChapterMetadata {
  val empty = ChapterMetadata("", 0, ChapterType.Chapter)
}

object ChapterType extends Enumeration {
  type ChapterType = Value
  val Foreword, Preface, Chapter, Appendix = Value
}

object ContentType extends Enumeration {
  type ContentType = Value
  val Text, Image, NewLine = Value
}
