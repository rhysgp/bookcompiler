package com.rhyssoft.bookcompiler.model

case class Chapter(
  metadata: ChapterMetadata,
  paragraphs: List[Paragraph]
)

object Chapter {
  def apply(paragraphs: List[Paragraph]): Chapter = Chapter(ChapterMetadata.empty, paragraphs)
}

