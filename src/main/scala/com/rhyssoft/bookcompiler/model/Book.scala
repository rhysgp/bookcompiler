package com.rhyssoft.bookcompiler.model

/**
  * Represents a book.
  */
case class Book(
  metadata: BookMetadata,
  chapters: List[Chapter]
)
