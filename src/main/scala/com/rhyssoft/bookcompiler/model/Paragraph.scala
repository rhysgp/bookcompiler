package com.rhyssoft.bookcompiler.model

import com.rhyssoft.bookcompiler.model.ParagraphStyle.ParagraphStyle

case class Paragraph(
  cssStyle: String,
  inlineContent: List[InlineContent],
  style: ParagraphStyle = ParagraphStyle.Normal,
  styleClass: Set[String] = Set()
)

object Paragraph{
  def apply(inlineContent: List[InlineContent], paraStyle: ParagraphStyle): Paragraph =
    Paragraph("", inlineContent, paraStyle)
  def separator = Paragraph("", Nil, ParagraphStyle.Separator)
  def txt(text: String) = Paragraph("", InlineContent.txt(text) :: Nil, ParagraphStyle.Normal)
  val empty = Paragraph("", Nil, ParagraphStyle.Normal)
}

object ParagraphStyle extends Enumeration {
  type ParagraphStyle = Value
  val Normal, Indented, Separator, Continuation = Value
}

