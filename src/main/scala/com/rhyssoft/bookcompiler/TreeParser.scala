package com.rhyssoft.bookcompiler

import java.io.Reader

import com.rhyssoft.htmlparser._

object TreeParser extends ElementParser with HtmlNodeSynthesiser {

  collectingText = true

  def parseText(reader: Reader, onlyWithinBody: Boolean): List[Node] = {

    collectNodes(reader, onlyWithinBody)
  }

  private def collectNodes(reader: Reader, bodyOnly: Boolean) = {
    var document = List[Node]()
    var withinBody = false
    val cbuf = Array[Char](8192)
    var done = false

    do {

      val count = reader.read(cbuf)

      if (count == -1) {
        done = true
      } else {

        for (i <- 0 until count) {
          val nodes = parse(cbuf(i))

          if (nodes != null && nodes.nonEmpty) {

            if (isEndBody(nodes.head)) {
              withinBody = false
            }

            if (!bodyOnly || withinBody) {
              document = nodes.reverse ::: document
            }

            if (isStartBody(nodes.head)) {
              withinBody = true
            }
          }
        }
      }

    } while (!done)

    document.reverse
  }

  def isStartBody(n: Node) = isStartOrEndBody(start = true, n)
  def isEndBody(n: Node) = isStartOrEndBody(start = false, n)


  def isStartOrEndBody(start: Boolean, n: Node) = {
    n match {
      case e: Element if e.name == "body" && e.isStartElement == start =>
        true
      case _ =>
        false
    }
  }
}
