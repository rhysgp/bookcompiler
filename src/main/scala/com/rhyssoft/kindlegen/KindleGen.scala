package com.rhyssoft.kindlegen

import java.nio.file.{Path, Paths}

import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory

object KindleGen {

  val logger = Logger(LoggerFactory.getLogger("com.rhyssoft.kindlegen.KindleGen"))
  val DEFAULT_KINDLE_GEN_PATH = "/Users/rhys/utils/KindleGen_Mac_i386_v2_9/kindlegen"

  val kindleGenPath = {
    var path = System.getProperty("kindlegen-path")
    if (path == null) {
      logger.info(s"kindlegen-path environment variable isn’t set. Defaulting to $DEFAULT_KINDLE_GEN_PATH")
      path = DEFAULT_KINDLE_GEN_PATH
    }
    path
  }

  def run(dir: String): Int = run(Paths.get(dir))

  def run(dir: Path): Int = {
    println("== kindlegen output ============================================================")
    val process = new ProcessBuilder()
      .command(kindleGenPath, "book.opf")
      .directory(dir.toFile)
      .redirectErrorStream(true)
      .start()

    val is = process.getInputStream

    var done = false
    val bytes = Array.ofDim[Byte](8192)
    while (!done) {
      val count = is.read(bytes)
      if (count == -1) {
        done = true
      } else if (count > 0) {
        print(new String(bytes, 0, count))
      }
    }

    process.waitFor()
    println("============================================================ kindlegen output ==")

    process.exitValue()
  }
}
