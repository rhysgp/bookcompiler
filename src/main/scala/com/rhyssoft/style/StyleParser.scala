/*
 * Copyright (c) 2014 White Clarke Group. All rights reserved.
 */

package com.rhyssoft.style

/**
 * Parses CSS style string into different name/value pairs.
 *
 * @author Rhys Parsons
 */
object StyleParser {

  def parse(style: String): Map[String,String] = {
    _parseStyleParts(_parseStyles(style))
  }

  def format(styles: Map[String,String]): String = {
    val sb = new StringBuilder()
    styles.foreach{
      case (k,v) =>
        if (sb.nonEmpty) {
          sb.append(';')
        }
        sb.append(k).append(':').append(v)
    }

    sb.toString
  }

  private def _parseStyleParts(styles: List[String]): Map[String,String] = {
    styles.map{ s =>
      val parts = s.split(":")
      (parts(0).trim, parts(1).trim)
    }.toMap
  }

  private def _parseStyles(style: String): List[String] = {

    var styles = List[String]()
    var inSingleQuotes = false
    var inDoubleQuotes = false
    var ampEscape = false
    val sb = new StringBuilder()

    style.foreach{ char =>

      if (char == '\'') {
        inSingleQuotes = !inSingleQuotes
      } else if (char == '"') {
        inDoubleQuotes = !inDoubleQuotes
      } else if (char == '&') {
        if (!inSingleQuotes && !inDoubleQuotes) {
          ampEscape = true
        }
      }

      if (char == ';') {
        if (ampEscape) {
          ampEscape = false
          sb.append(char)
        } else {
          styles = sb.toString :: styles
          sb.setLength(0)
        }
      } else {
        sb.append(char)
      }
    }

    if (sb.nonEmpty) {
      styles = sb.toString :: styles
    }

    styles.reverse
  }


  private def parseSingleStyle(style: String): (String,String) = {

    val elements = style.split(":")

    (elements(0), elements(1))
  }
}
