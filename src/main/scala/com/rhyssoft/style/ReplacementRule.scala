/*
 * Copyright (c) 2014 Rhys Parsons. All rights reserved.
 */

package com.rhyssoft.style

import com.rhyssoft.htmlparser.ElementType.ElementType

sealed trait Rule

case class RemovalRule(forElementTypes: Set[ElementType], cssProperty: String, cssPropertyValue: String = null) extends Rule

case class ReplacementRule(forElementTypes: Set[ElementType],
                           cssProperty: String,
                           cssPropertyValue: String = null,
                           replaceWithProperty: String = null,
                           replaceWithPropertyValue: String = null) extends Rule


