/*
 * Copyright (c) 2014 Rhys Parsons. All rights reserved.
 */

package com.rhyssoft.opf

import com.rhyssoft.bookcompiler.ChapterFile
import com.rhyssoft.bookcompiler.model.Author

/*
 * Classes for specifying package data.
 */

case class OpfPackage(metaData: OpfMetaData, manifest: OpfManifest, spine: OpfSpine, guide: OpfGuide) {
  def toXml =
    <package version="2.0" xmlns="http://www.idpf.org/2007/opf">
      {metaData.toXml}
      {manifest.toXml}
      {spine.toXml}
      {guide.toXml}
    </package>
}

case class OpfMetaData(title: String, language: String, author: String, authorFileAs: String, coverFilename: String) {
  def toXml =
    <metadata xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:opf="http://www.idpf.org/2007/opf">
      <dc:title>{title}</dc:title>
      <dc:language>{language}</dc:language>
      <dc:creator opf:file-as={authorFileAs} opf:role="aut">{author}</dc:creator>

      <x-metadata>
        <EmbeddedCover>{coverFilename }</EmbeddedCover>
      </x-metadata>

    </metadata>
}

case class OpfManifest(items: Seq[ManifestItem]) {
  def toXml =
    <manifest>
      {
      items.map(_.toXml)
      }
    </manifest>
}

case class ManifestItem(id: String, href: String, mediaType: String) {
  def toXml =
    <item id={id} href={href} media-type={mediaType} />
}

case class SpineItemRef(idRef: String) {
  def toXml =
    <itemref idref={idRef} />
}

case class OpfSpine(references: Seq[SpineItemRef]) {
  def toXml =
    <spine toc="toc">
      {
      references.map(_.toXml)
      }
    </spine>
}

case class GuideReference(_type: String, title: String, href: String) {
  def toXml =
    <reference type={_type} title={title} href={href} />
}

case class OpfGuide(references: Seq[GuideReference]) {
  def toXml =
    <guide>
      {
      references.map(_.toXml)
      }
    </guide>
}

object Opf {
  def createOpfPackage(title: String, language: String, author: Author, coverFilename: String, chapters: Seq[ChapterFile]) = {
    val manifestItems =
      ManifestItem("ncx", "toc.ncx", "application/x-dtbncx+xml") ::
        ManifestItem("table_of_contents", "toc.html", "application/xhtml+xml") ::
        chapters.toList.zipWithIndex.map{ case (chapter, i) =>
          ManifestItem("chapter" + i, chapter.filename, "application/xhtml+xml")
        }

    val spineItems = SpineItemRef("table_of_contents") :: SpineItemRef("chapter0") :: Nil

    OpfPackage(
      OpfMetaData(title, language, author.toString, author.fileAs, coverFilename),
      OpfManifest(manifestItems),
      OpfSpine(spineItems),
      OpfGuide(
        Seq(
          GuideReference("toc", "Table of Contents", "toc.html")
        )
      )
    )

  }
}