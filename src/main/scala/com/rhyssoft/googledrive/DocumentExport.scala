package com.rhyssoft.googledrive

import java.io.{InputStreamReader, OutputStream, File => JFile}
import java.nio.file.{Files, Path, Paths}
import java.util

import com.google.api.client.auth.oauth2.Credential
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver
import com.google.api.client.googleapis.auth.oauth2.{GoogleAuthorizationCodeFlow, GoogleClientSecrets}
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport
import com.google.api.client.json.jackson2.JacksonFactory
import com.google.api.client.util.store.FileDataStoreFactory
import com.google.api.services.drive.model.{File => GFile}
import com.google.api.services.drive.{Drive, DriveScopes}
import com.rhyssoft.IOUtils._

import scala.collection.JavaConversions._
import scala.util.Try

class DocumentExport{}

object DocumentExport {

  private[googledrive] val homeDir = System.getProperty("user.home")
  private val APPLICATION_NAME = "Book Compiler"
  private val DATA_STORE_DIR = new JFile(homeDir, ".credentials/Book_Compiler")

  private val HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport()
  private val DATA_STORE_FACTORY = new FileDataStoreFactory(DATA_STORE_DIR)

  private val JSON_FACTORY = JacksonFactory.getDefaultInstance
  private val SCOPES = util.Arrays.asList(DriveScopes.DRIVE)

  private val folder = Option("application/vnd.google-apps.folder")

  def export(drivePath: Seq[String], outputDir: Path): Seq[DownloadedFile] = {
    driveService().map(drive => {
      listPathEntries(drive, drivePath)
        .zipWithIndex
        .map{ case(f, i) =>
          val destFile = withFileOutputStream(outputDir, s"${f.getName}.html")(exportFile(drive, f, _))
          DownloadedFile(f.getName, destFile)
        }
    }).getOrElse(Seq())
  }

  private def authorize(): Credential = {
    val secret = classOf[DocumentExport].getClassLoader.getResourceAsStream("client_secret.json")
    val clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(secret))
    val flow =
      new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
        .setDataStoreFactory(DATA_STORE_FACTORY)
        .setAccessType("offline")
        .build()

    val credential = new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user")
    println(s"Credential saved to ${DATA_STORE_DIR.getAbsolutePath}")
    credential
  }

  private def driveService(): Option[Drive] = {
    val t = Try {
      val credential = authorize()
      new Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential)
        .setApplicationName(APPLICATION_NAME)
        .build()
    } recover {
      case t: Throwable =>
        t.printStackTrace()
        throw t
    }

    t.toOption
  }

  private def list(drive: Drive, query: Query): Seq[GFile] = {
    val files = drive.files().list()
      .setPageSize(1000)
      .setFields("files(id, name)")
      .setOrderBy("name")
      .setQ(query.q)
      .execute()
      .getFiles
      .toList

    println(s"list(${query.q}): files are:")
    files.foreach(f => println(s"\t${f.getName}"))

    files
  }

  private def buildQuery(title: Option[String] = None, parent: Option[String] = None, mimeType: Option[String] = None): Query = {
    val titleClause = title.filterNot(_.trim.length == 0).map(t => s"name contains '$t'")
    val parentClause = parent.filterNot(_.trim.length == 0).map(p => s"'$p' in parents")
    val mimeTypeClause = mimeType.filterNot(_.trim.length == 0).map(mt => s"mimeType = '$mt'")
    val explicitlyTrashedClause = Option("explicitlyTrashed = false")

    Query(
      Seq(titleClause, parentClause, mimeTypeClause, explicitlyTrashedClause)
        .filter(_.nonEmpty)
        .map(_.get)
        .mkString(" and ")
    )
  }

  private def listPathEntries(drive: Drive, path: Seq[String]): Seq[GFile] = {
    val parentIdOpt = path.foldLeft(None: Option[String])((parent, pathItem) => {
      list(drive, buildQuery(title = Option(pathItem), parent = parent, mimeType = folder))
        .headOption
        .map(_.getId)
    })

    parentIdOpt
      .map(id => list(drive, buildQuery(parent = Option(id))))
      .getOrElse(Seq())
  }

  private def exportFile(drive: Drive, f: GFile, outputStream: OutputStream): OutputStream = {
    drive.files().export(f.getId, "text/html").executeMediaAndDownloadTo(outputStream)
    outputStream
  }

  private def withFileOutputStream(outputDir: Path, filename: String)(f: OutputStream => Unit): JFile = {
    val outputFile = new JFile(outputDir.toFile, filename)
    autoClose(Files.newOutputStream(outputDir.resolve(filename)))(f)
    outputFile
  }
}

case class Query(q: String)
case class DownloadedFile(title: String, file: JFile)

object DocumentExportApp extends App {

  import DocumentExport._

  if (checkArgs(args)) {
    export(Seq("Ash and the Egyptian Sarcophagus", "Draft 3"), Paths.get(args(0)))
      .foreach(df => println(s"Written ${df.file.getAbsolutePath}"))
  } else {
    println("Please supply an output directory as a command-line parameter.")
  }

  def checkArgs(args: Array[String]): Boolean = {
    args.length == 1 && (new JFile(args(0)).isDirectory || new JFile(args(0)).mkdirs())
  }
}
