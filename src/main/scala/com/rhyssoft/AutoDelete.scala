package com.rhyssoft

import java.nio.file.{Files, Path}

import scala.util.Try

object AutoDelete {

  private var paths: List[Path] = Nil

  Runtime.getRuntime.addShutdownHook(new Thread() {
    override def run(): Unit = {
      paths.foreach(p =>
        Try (Files.delete(p)) recover {
          case t: Exception =>
            println(s"Attempting to delete '$p' resulted in ${t.getMessage}")
        }
      )
    }
  })

  def add(path: Path): Unit = {
    paths = path :: paths
  }

}
