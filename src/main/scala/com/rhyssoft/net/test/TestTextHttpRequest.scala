/*
 * TestTextHttpRequest.scala
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.rhyssoft.net.test

import com.rhyssoft.net._

object TestTextHttpRequest {

  def main(args: Array[String]): Unit = {

    def handleChars(chars: Array[Char], count: Int): Unit = {
      var i = 0
      for (i <- 0 to count-1) {
        print(chars(i))
      }
    }

    new TextHttpRequest("http://www.rhyssoft.com",
                        "UTF-8",
                        (code: Int) => println("response code: " + code),
                        handleChars,
                        () => println("Done!")
    )
  }
}
