/*
 * HttpRequest.scala
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.rhyssoft.net

import java.net._
import java.io._

/**
 * Based on the JavaFX version of HttpRequest.
 */
class TextHttpRequest(val url: String,
                      val defaultCharset: String,
                      onResponseCode: (Int) => Unit,
                      onReader: (Reader) => Unit,
                      onChars: (Array[Char], Int) => Unit,
                      onDone: () => Unit) {

  def this(url: String, defaultCharset: String, onResponseCode: (Int) => Unit, onReader: (Reader) => Unit) = this(url, defaultCharset, onResponseCode, onReader, null, null)
  def this(url: String, defaultCharset: String, onResponseCode: (Int) => Unit, onChars: (Array[Char], Int) => Unit, onDone: () => Unit) = this(url, defaultCharset, onResponseCode, null, onChars, onDone)

  private val urlObject: URL = new URL(url)
  private val connection: HttpURLConnection = urlObject.openConnection().asInstanceOf[HttpURLConnection]
  private var charset: String = null
  private var contentLength: Int = -1
  private var inputStream: InputStream = null
  private var reader: InputStreamReader = null

  onResponseCode(connection.getResponseCode)

  parseHeaders(connection)

  if (charset == null) {
    charset = defaultCharset
  }

  inputStream = connection.getInputStream

  reader = new InputStreamReader(inputStream, charset)

  if (onReader != null) {
    onReader(reader)
  }

  if (onChars != null) {
    readFromStream()
  }


  private def readFromStream(): Unit = {
    // read some bytes, then event them:
    var done = false
    while (!done) {
      val chars = new Array[Char](8192)
      val count = reader.read(chars)

      if (count == -1) {
        done = true
      } else {
        onChars(chars, count)
      }
    }

    onDone()
  }

  private def parseHeaders(connection: HttpURLConnection): Unit = {

    def parseContentTypeForCharset(contentType: java.util.List[String]): String = {
      val ct = contentType.get(0)
      val index = ct.indexOf("charset=")
      if (index >= 0) {
        ct.substring(index + 8).trim
      } else {
        null
      }
    }

    def parseContentLength(contentLength: java.util.List[String]): Int = {
      if (contentLength != null && contentLength.size == 1) {
        contentLength.get(0).toInt
      } else {
        -1
      }
    }

    if (connection.getResponseCode == 200) {

      val headers = connection.getHeaderFields

      // find Connection-Type header:
      val keys: java.util.Iterator[String] = headers.keySet.iterator
      while (keys.hasNext) {
        val key = keys.next
        if (key == "Content-Type") {
          charset = parseContentTypeForCharset(headers.get(key))
        } else if (key == "Content-Length") {
          contentLength = parseContentLength(headers.get(key))

        }
      }

      //val reader = new InputStreamReader(connection.getInputStream, charset)

      //

    } else {
      // handle error!
      println("Got response code " + connection.getResponseCode)
    }
  }
}
