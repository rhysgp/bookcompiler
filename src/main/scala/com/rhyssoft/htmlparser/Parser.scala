/*
 * Parser.scala
 *
 * Copyright (c) 2009 Rhys Parsons. All rights reserved.
 * Use of this code is strictly prohibited without written permission from the
 * author.
 */

package com.rhyssoft.htmlparser

import scala.collection.Set

class Parser (baseRule: ElementRule, reader: java.io.Reader) extends NodeSynthesiser with ElementParser with HtmlNodeSynthesiser {

  private var nodesToProcess: List[Node] = List()

  executeForElementRules(Set(baseRule))

  private def executeForElementRules(rules: Set[ElementRule]) {

    def processRules(node: Node, level: Int): Int = {
      var l: Int = level

      rules.foreach(rule =>
        if (rule.matches(node)) {

          rule.fireEvent(EventType.StartElement, node.asInstanceOf[Element])

          if (rule.eventType == EventType.Text) {
            rule.fireEvent(EventType.Text, parseText())
            l -= 1 // because parseText() will have consumed an end tag
          } else if (rule.eventType == EventType.SubElementList) {
            blindlyConsumeContent() // TODO: parse sub-elements and return text as a list of strings...
            l -= 1 // because the above will have consumed an end tag
          } else if (rule.eventType == EventType.Content) {
            blindlyConsumeContent() // TODO: not implemented returning all content of an element...
            l -= 1 // because the above will have consumed an end tag
          } else if (node.isInstanceOf[Element] && !node.asInstanceOf[Element].empty) {
            executeForElementRules(rule.subRules)
            l -= 1 // because the above will have consumed an end tag
          }

          rule.fireEvent(EventType.EndElement)
        }
      )
      l
    }

    execute(processRules)
  }

  private def blindlyConsumeContent() {

    def noop(node: Node, level: Int): Int = level

    execute(noop)
  }

var womble: Int = 0

  /**
   * Read nodes for the current level and pass any nodes to the supplied function.
   */
  private def execute(f: (Node, Int) => Int) {
    var level = 0
    var eof = false

//println(">execute()")
    do {
      while (nodesToProcess.size > 0 && level >= 0) {
        val node = nodesToProcess.head
        //val node = nodesToProcess.remove(0)
        nodesToProcess = nodesToProcess drop 1
        if (node.isInstanceOf[Element]) {
          val element = node.asInstanceOf[Element]
          if (element.isEndElement) {
            womble -= 1
//            for (i <- 0 to womble) print(' ')
//            println("processing end element "  + node.asInstanceOf[Element].name)
          } else if (!element.empty) {
//            for (i <- 0 to womble) print(' ')
//            println("processing start element " + node.asInstanceOf[Element].name)
            womble += 1
          }
        }
        level = f(node, level)

        level += levelDelta(node)
//        println("level: " + level)
      }

      if (level >= 0) {
        val value = reader.read()

        eof = value == -1

        if (!eof) {
          val node = parse(value.asInstanceOf[Char])
          if (node != null) {
//            nodesToProcess.addAll(node)
            nodesToProcess = nodesToProcess ::: node
          }
        }
      }

    } while (!eof && level >= 0)
//println("<execute()")
  }

  /**
   * Parses text parts.
   */
  private def parseText(): List[Text] = {
    var textNodes: List[Text] = List()

    def processNode(node: Node, level: Int): Int = {
      if (node.isInstanceOf[Text]) {
        textNodes = textNodes ::: List(node.asInstanceOf[Text])
      }

      level
    }

    collectingText = true
    execute(processNode)
    collectingText = false

    textNodes
  }

  private def levelDelta(node: Node): Int =
    if (node == null || !node.isInstanceOf[Element]) {
      0
    } else if (node.asInstanceOf[Element].isEndElement) {
      -1
    } else if (node.asInstanceOf[Element].empty) {
      0
    } else {
      1
    }

}
