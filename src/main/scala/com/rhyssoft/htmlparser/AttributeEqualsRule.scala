/*
 * AttributeEqualsRule.scala
 *
 * Copyright (c) 2009 Rhys Parsons. All rights reserved.
 * Use of this code is strictly prohibited without written permission from the
 * author.
 */

package com.rhyssoft.htmlparser

import scala.collection.Map;

class AttributeEqualsRule(name: String, value: String) extends AttributeRule(name, value, null) {

  override def matches(element: Element): Boolean = {

    val map: Map[String,String] = element.attributes

    if (element.attributes contains name)
      map(name) == value
    else
      false
  }
}
