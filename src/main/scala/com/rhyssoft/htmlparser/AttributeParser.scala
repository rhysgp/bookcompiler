/*
 * AttributeParser.scala
 *
 * Copyright (c) 2009 Rhys Parsons. All rights reserved.
 * Use of this code is strictly prohibited without written permission from the
 * author.
 */

package com.rhyssoft.htmlparser

object AttributeParser {

  type ParserFunc = (Char) => Unit

  def parseAttributes(attrStream: String): Map[String, String] = {

    var attributes = Map[String,String]()
    var parser: ParserFunc = null
    var attrName: StringBuilder = new StringBuilder
    var attrValue: StringBuilder = new StringBuilder

    var quotedValue = false
    var quoteChar = '"'
    var escaped = false

    def expectStartAttrName(char: Char) {
      if (!char.isWhitespace) {
        attrName += char
        parser = expectEndAttrName
      }
    }

    def expectEndAttrName(char: Char) {
      if (char == '=') {
        parser = expectStartAttrValue
      } else if (char.isWhitespace) {
        parser = expectAttrEquals
      } else {
        attrName += char
      }
    }

    def expectAttrEquals(char: Char) {
      if (char == '=') {
        parser = expectStartAttrValue
      } else if (char.isWhitespace) {
        // do nothing
      } else {
        // assume that we have an empty attribute:
        // add previous and reset:
        attributes = attributes + (attrName.toString -> "")
        attrName.clear()
        attrValue.clear()
        parser = expectStartAttrName
      }
    }

    def expectStartAttrValue(char: Char) {
      if (!char.isWhitespace) {
        if (char == '"' || char == '\'' ) {
          quotedValue = true
          quoteChar = char
        } else {
          quotedValue = false
          attrValue += char
        }
        parser = expectEndAttrValue
      }
    }

    def expectEndAttrValue(char: Char) {

      def handleValueEnd(): Unit = {
        parser = expectStartAttrName
        attributes += (attrName.toString -> attrValue.toString)
        attrName.clear()
        attrValue.clear()
      }

      if (quotedValue) {
        if (escaped) {
          attrValue += char
          escaped = false
        } else {
          if (char == '\\') {
            escaped = true
          } else if (char == quoteChar) {
            handleValueEnd()
          } else {
            attrValue += char
          }
        }
      } else {
        if (char.isWhitespace) {
          handleValueEnd()
        } else {
          attrValue += char
        }
      }
    }

    parser = expectStartAttrName
    attrStream.foreach((c: Char) => parser(c))
    parser(' ') // flush!

    attributes
  }
}
