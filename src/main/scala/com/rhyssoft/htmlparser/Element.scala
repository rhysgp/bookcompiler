/*
 * Element.scala
 *
 * Copyright (c) 2009 Rhys Parsons. All rights reserved.
 * Use of this code is strictly prohibited without written permission from the
 * author.
 */

package com.rhyssoft.htmlparser

/**
 * Represents either an element start or an element end.
 */
case class Element(name: String, attributes: Map[String,String], isEndElement: Boolean, empty: Boolean) extends Node {

  val isStartElement = !isEndElement

  private def attrToStr(attr: Map[String,String]) = {

    val sb = new StringBuffer()

    if (attributes != null) {
      attributes.foreach{ case(key,value) =>
        if (sb.length() == 0) {
          sb.append(" ")
        }
        sb.append(s"""$key="$value"""")
      }
    }

    sb.toString
  }

  override def toString = s"<${if (isEndElement) "/" else ""}$name${attrToStr(attributes)}${if (empty) "/" else ""}>"
}

object Element {

  def apply(name: String, attributes: String, isEndElement: Boolean, empty: Boolean): Element =
    Element(name, AttributeParser.parseAttributes(attributes), isEndElement, empty)

  def apply(name: String, attr: String, isEndElement: Boolean): Element =
    apply(name, attr, isEndElement, empty = false)

  def start(name: String): Element =
    Element(name, Map.empty[String,String], isEndElement = false, empty = false)

  def start(name: String, attributes: Map[String,String]): Element =
    Element(name, attributes, isEndElement = false, empty = false)

  def start(name: String, attributes: String): Element =
    start(name, AttributeParser.parseAttributes(attributes))

  def end(name: String): Element =
    Element(name, Map.empty[String,String], isEndElement = true, empty = false)

  def end(name: String, attributes: Map[String,String]): Element =
    Element(name, attributes, isEndElement = true, empty = false)

  def end(name: String, attributes: String): Element =
    end(name, AttributeParser.parseAttributes(attributes))

  def empty(name: String, attributes: Map[String,String]): Element =
    Element(name, attributes, isEndElement = false, empty = true)

  def empty(name: String, attributes: String): Element =
    Element(name, attributes, isEndElement = false, empty = true)

  def empty(name: String): Element =
    empty(name, Map.empty[String,String])
}
