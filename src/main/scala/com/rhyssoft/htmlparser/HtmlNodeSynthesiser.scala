/*
 * HtmlNodeSynthesiser.scala
 *
 * Copyright (c) 2009 Rhys Parsons. All rights reserved.
 * Use of this code is strictly prohibited without written permission from the
 * author.
 */

package com.rhyssoft.htmlparser

import scala.collection.mutable

trait HtmlNodeSynthesiser extends NodeSynthesiser {

  val stack = mutable.Stack[Element]()

  abstract override protected def synthesiseMissingNodes(node: Node): List[Node] = {

    /*
     * This trait keeps a stack of nodes so that we can insert 'missing' nodes where required.
     * This is necessary when processing HTML that's not XHTML.
     *
     * In particular, we're interested in the following scenario:
     *
     * <table>
     *   <tr><td>one<td>two<td>three
     *   <tr><td>one<td>two<td>three
     * </table>
     *
     * Note that in the above example, the </td> and </tr> have to be inferred - this method needs
     * to insert those inferred end nodes.
     *
     * Consider also the following scenario:
     *
     * <table>
     *   <tr><td><p>Hello<p>Hi
     *   <tr><td>womble
     * </table>
     *
     * Note that after the text 'Hi' in the first row, we need to insert three inferred end nodes:
     * </p></td></tr>. This will all have to be inferred from the fact that the <tr> on the second row
     * cannot be contained either within a <p> section or a <td> section or a <tr> section!
     *
     * For the initial cut, we only have to cover the above scenarios: we don't need an exhaustive list
     * of which elements can be contained in which other elements! - at least, not yet...!
     */


    var retVal : List[Node] = super.synthesiseMissingNodes(node)
    var synthesisedElements : List[Node] = List()

    if (node.isInstanceOf[Element]) {
      var element = node.asInstanceOf[Element]

      if (!element.isEndElement && !element.empty && empty(element)) {
        element = element.copy(empty = true)
      }

      if (element.isEndElement) {

        var poppedElement = false

        while(!poppedElement) {
          val topNode = stack.pop()
          if (topNode.name == element.name) {
              poppedElement = true
//              println("Popping " + element.name + " off the stack!")
          } else {
            // TODO Is this a bug? - it's not doing anything!
            synthesisedElements ::: List(Element(topNode.name, Map.empty[String,String], isEndElement = true, empty = false))
//            println("-->[0]Syntehsised end element for " + topNode.name)
          }
        }
      } else if (!element.empty) {

        // if the element we're trying to push can't be contained by the previous
        // element in the stack, then we have an unterminated element. In this case
        // we need to synthesise the end element.

        while (stack.nonEmpty && !containmentIsAllowed(element, stack.top)) {
          // synthesise an end element:
          val elementEndtoSynthesise = stack.pop()
          //retVal = Element(elementEndtoSynthesise.name, null, true, false) :: retVal
          synthesisedElements = synthesisedElements ::: List(Element(elementEndtoSynthesise.name, Map.empty[String,String], true, false))
//          println("Popping " + elementEndtoSynthesise.name + " off the stack (synthesised due to " + element.name + "!)")
        }

        stack.push(element)

        retVal = synthesisedElements ++ retVal

//        println("Pushing " + element.name + " onto the stack!")
      }
    }

    retVal
  }

  private def containmentIsAllowed(element: Element, parentElement: Element): Boolean =
      if (element.name == "li" && !parentElement.isEndElement && parentElement.name == "li") {
        false
      } else if ((element.name == "td" || element.name == "th") && (parentElement.name == "td" || parentElement.name == "th")) {
        false
      } else if (element.name == "tr" && (parentElement.name == "tr" || parentElement.name == "td" || parentElement.name == "th")) {
        false
      } else if (element.name == "p" && !parentElement.isEndElement && parentElement.name == "p") {
        false
      } else {
        true
      }

  private def empty(element: Element): Boolean = {

    val emptyList = List("meta", "br", "link", "img", "input")

    emptyList contains element.name.toLowerCase
  }
}
