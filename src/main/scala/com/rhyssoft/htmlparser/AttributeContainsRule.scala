/*
 * AttributeContainsRule.scala
 *
 * Copyright (c) 2009 Rhys Parsons. All rights reserved.
 * Use of this code is strictly prohibited without written permission from the
 * author.
 */

package com.rhyssoft.htmlparser

class AttributeContainsRule(name: String, value: String) extends AttributeRule(name, value, null) {

  override def matches(element: Element): Boolean = {
    val map = element.attributes
    if (element.attributes contains name)
      containsValue(map(name))
    else
      false
  }

  private def containsValue(v: String): Boolean = {
    val values = v.split(' ')
    return values contains value
  }
}
