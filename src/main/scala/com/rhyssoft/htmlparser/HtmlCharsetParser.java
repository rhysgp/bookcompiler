/*
 * Copyright (c) 2009 Rhys Parsons. All rights reserved.
 * Copying this source without the express, written permission of the author is strictly prohibited.
 */

package com.rhyssoft.htmlparser;

/**
 * Parses the stream of characters for an HTTP-EQUIV META element.
 *
 * @author Rhys Parsons
 */
public class HtmlCharsetParser {

    private State state = State.WaitingForBeginAngleBracket;
    private String charset;
    private StringBuilder sb = new StringBuilder();

    enum State {
        WaitingForBeginAngleBracket,
        StartDecodingTag,
        DecodingTag,
        StartDecodingAttrName,
        DecodingAttrName,
        StartDecodingAttrValue,
        DecodingAttrValue
    }

    public boolean parse(char ch) {

        boolean stillParsing = false;

        switch (state) {
            case WaitingForBeginAngleBracket:
                if (ch == '<') {
                    state = State.StartDecodingTag;
                }
                break;
            case StartDecodingTag:
                sb.setLength(0);
                if (Character.isLetter(ch)) {
                    if (!Character.isWhitespace(ch)) {
                        state = State.DecodingTag;
                    }
                } else if (ch == '<') {
                    // no-op: accidental multiple-open-angled-bracket?
                } else {
                    state = State.WaitingForBeginAngleBracket;
                }
                break;
            case DecodingTag:
                if (Character.isLetter(ch)) {
                    if (Character.isWhitespace(ch)) {
                        if (sb.toString().toLowerCase().equals("meta")) {
                            state = State.StartDecodingAttrName;
                        } else {
                            state = State.WaitingForBeginAngleBracket;
                        }
                    } else {
                        sb.append(ch);
                    }
                } else { // big screw up in HTML:
                    if (ch == '<') {
                        state = State.StartDecodingTag;
                    } else {
                        state = State.WaitingForBeginAngleBracket;
                    }
                }
                break;
            case StartDecodingAttrName:
                sb.setLength(0);
                if (Character.isLetter(ch)) {
                    if (!Character.isWhitespace(ch)) {
                        sb.append(ch);
                        state = State.DecodingAttrName;
                    }
                }
                break;
            case DecodingAttrName:
                
                break;
        }

        return stillParsing;
    }
}
