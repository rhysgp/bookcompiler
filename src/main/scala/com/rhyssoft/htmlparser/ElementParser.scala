/*
 * ElementParser.scala
 *
 * Copyright (c) 2009 Rhys Parsons. All rights reserved.
 * Use of this code is strictly prohibited without written permission from the
 * author.
 */

package com.rhyssoft.htmlparser

// TODO: handle comments
// TODO: handle processing instructions
// TODO: handle quoted attribute values that contain '<' '>' or '/'

trait ElementParser extends NodeSynthesiser {

  type ParserFunc = (Char) => Node
  
  private var parser: ParserFunc = expectingOpenAngleBracket
  private var elementName: StringBuilder = new StringBuilder
  private var attributes: StringBuilder = new StringBuilder
  private var text: StringBuilder = new StringBuilder
  private var endElement: Boolean = false
  protected var collectingText : Boolean = false

  /**
   * Parses the character.
   */
  def parse(char: Char): List[Node] = synthesiseMissingNodes(parser(char))

  /**
   * Synthesises any missing nodes.
   */
  protected def synthesiseMissingNodes(node: Node): List[Node] =
    if (node == null) {
      List[Node]()
    } else {
      List(node)
    }


  private def expectingOpenAngleBracket(char: Char): Node = {
    var retVal: Node = null
    if (char == '<') {
      elementName.clear()
      attributes.clear()
      parser = expectingStartTagName
      endElement = false
      if (collectingText && text.nonEmpty) {
        retVal = Text(text.toString())
        text.setLength(0)
      }
    } else {
      if (collectingText) {
        text += char
      }
      parser = expectingOpenAngleBracket // just consume bytes till we've got the '<' we want
    }
    retVal
  }

  private def expectingStartTagName(char: Char): Node = {
    if (isValidNameStartChar(char) || char.isWhitespace) {
      elementName += char
      parser = expectingEndTagName       // okay: got start of tag, now expect the end
    } else if (char == '!') {
      parser = expectingComment1         // could be comment or DOCTYPE
    } else if (char == '<') {
      parser = expectingStartTagName     // Error: ignore previous angled bracket as a typo
    } else if (char.isWhitespace) {
      parser = expectingStartTagName     // this is okay: just carry on as before
    } else if (char == '/') {
      endElement = true
    } else {
      parser = expectingOpenAngleBracket // Error: start looking for open angled bracket again
    }
    null
  }

  private def isValidNameStartChar(char: Char): Boolean = {
    char.isLetter || char == ':' || char == '_'
  }

  private def isValidNameChar(char: Char): Boolean = {
    char == '-' || char == '.' || char.isDigit || char == 0xB7 || isValidNameStartChar(char)
  }

  private def expectingEndTagName(char: Char): Node = {
    if (char == '>') {
      parser = expectingOpenAngleBracket
      Element(elementName.toString().trim, attributes.toString(), endElement)
    } else if (char.isWhitespace) {
      parser = expectingAttributes
      null
    } else if (char == '/') {
      parser = expectingEmptyElementCloseAngleBracket
      null
    } else if (isValidNameChar(char)) {
      elementName += char
      null
    } else {
      // it wasn't really an element: treat it as though the text was just text!
      parser = expectingStartTagName
      if (collectingText) {
        text ++= elementName
        elementName.setLength(0)
      }
      null
    }
  }

  private def expectingEmptyElementCloseAngleBracket(char: Char): Node = {
    if (char == '>') {
      parser = expectingOpenAngleBracket
      Element(elementName.toString().trim, attributes.toString(), isEndElement =  false, empty = true)
    } else {
      // Not quite sure what to do here!?
      parser = expectingOpenAngleBracket
      null
    }
  }

  private def expectingAttributes(char: Char): Node = {
    if (char == '>') {
      parser = expectingOpenAngleBracket
      Element(elementName.toString(), attributes.toString(), endElement)
    } else {
      attributes += char
      null
    }
  }

  private def expectingComment1(char: Char): Node = {
    if (char == '-') {
      parser = expectingStartTagName
    } else if (char == 'D') {
      //parser = expectingDoctype // TODO NEED TO IMPLEMENT expectingDoctype
    } else {
      parser = expectingOpenAngleBracket
    }
    null
  }
}