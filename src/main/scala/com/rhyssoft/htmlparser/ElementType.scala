/*
 * Copyright (c) 2014 White Clarke Group.
 */

package com.rhyssoft.htmlparser

/**
 * Element type.
 */
object ElementType extends Enumeration {

  type ElementType = Value

  val Inline, Block = Value
}
