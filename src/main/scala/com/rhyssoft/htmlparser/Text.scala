/*
 * Text.scala
 *
 * Copyright (c) 2009-2016 Rhys Parsons. All rights reserved.
 * Use of this code is strictly prohibited without written permission from the
 * author.
 */

package com.rhyssoft.htmlparser

import scala.annotation.tailrec

/**
 * Represents a text node.
 */
case class Text(rawText: String) extends Node {

  lazy val text = {
    val txt = replaceHashNumberEntities(rawText)
    printMissedEntities(txt)
    txt
  }

  private val hashEntityRegex = "(.*?)&#(x?[0-9]{1,4});(.*)".r
  private val namedEntityRegex = "(.*?)&([0-9a-zA-Z]{1,20});(.*)".r
  private val entityRegex = "&#?[A-Za-z0-9]{1,20};".r
  private def printMissedEntities(txt: String): Unit = {
    entityRegex.findAllIn(txt).toList.distinct
      .foreach((s: String) => println(s"!! Missing entity $s !!"))
  }

  @tailrec
  private def replaceHashNumberEntities(s: String): String = {
    val s2 = s match {
      case hashEntityRegex(before, code, after) =>
        if (code.startsWith("x")) {
          before + Integer.parseInt(code.substring(1), 16).toChar + after
        } else {
          before + code.toInt.toChar + after
        }

      case namedEntityRegex(before, code, after) =>
        before + lookup(code) + after

      case str => str
    }

    if (s2 == s) s else replaceHashNumberEntities(s2)
  }

  override def toString = text

  private def lookup(code: String): String = {
    codes.getOrElse(code, s"&$code;")
  }

  val codes = Map[String,String](
    "ndash" -> "–",
    "acirc" -> "â",
    "lsquo" -> "‘",
    "lt" -> "<",
    "gt" -> ">",
    "ldquo" -> "“",
    "rdquo" -> "”",
    "amp" -> "&",
    "nbsp" -> "\u00A0",
    "rsquo" -> "’",
    "AElig" -> "Æ",
    "aelig" -> "æ",
    "hellip" -> "…",
    "eacute" -> "é",
    "Eacute" ->	"É",
    "agrave" -> "à",
    "mdash" -> "—"
  )
}
