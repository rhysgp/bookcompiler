/*
 * Copyright (c) 2009 Rhys Parsons. All rights reserved.
 * Copying this source without the express, written permission of the author is strictly prohibited.
 */

package com.rhyssoft.htmlparser;

/**
 *
 * @author Rhys Parsons
 */
public enum EventType {
    StartElement,  // evented when the opening element has been parsed: this is used when sub-elements need to be parsed too
    EndElement,    // evented when the closing element has been parsed
    Text,          // parses the contents of the element and returns the text nodes
    Content,       // parses the contents of the element and returns the HTML as raw text
    SubElementList // parses the contents and returns the text portion of a specified sub-element (e.g. TD elements of a TR)
}
