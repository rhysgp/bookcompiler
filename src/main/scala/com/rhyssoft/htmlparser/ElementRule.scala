/*
 * ElementParsingRule.scala
 *
 * Copyright (c) 2009 Rhys Parsons. All rights reserved.
 * Use of this code is strictly prohibited without written permission from the
 * author.
 */

package com.rhyssoft.htmlparser

import scala.collection.Set

/**
 * Parses an element. We parse the complete opening of the element; then,
 * depending on whether we have sub-elements or not, we either continue reading
 * till we've parsed the end element and potentially event the contents of the
 * element, or we start to pass any element to sub-elements.
 *
 * Note that this element parser is likely to parse elements that it's not
 * really interested in. This element parser will throw these away until it has
 * satisfied its criteria (element name and attributes).
 */
class ElementRule(targetElement: String,
                  attributeRule: AttributeRule,
                  val eventType: EventType,
                  val subRules: Set[ElementRule],
                  onStartElement: (Element) => Unit,
                  onEndElement: () => Unit,
                  onText: (List[String]) => Unit,
                  onContent: (String) => Unit,
                  onList: (List[String]) => Unit) extends AnyRef {
  var enabled: Boolean = true

  def fireEvent(et: EventType): Unit = fireEvent(et, null, List())
  def fireEvent(et: EventType, element: Element): Unit = fireEvent(et, element, List())
  def fireEvent(et: EventType, nodes: List[Node]): Unit = fireEvent(et, null, nodes)

  def fireEvent(et: EventType, element: Element, nodes: List[Node]): Unit = {

    def convertTextNodeListToStringList(): List[String] = {
        for (node <- nodes) yield node.asInstanceOf[Text].text
    }

    if (et == EventType.StartElement) {
      if (onStartElement != null)
        onStartElement(element)
    } else if (et == EventType.EndElement) {
      if (onEndElement != null)
        onEndElement()
    } else if (et == EventType.Text) {
      if (onText != null)
        onText(convertTextNodeListToStringList())
    } else if (et == EventType.Content) {
      if (onContent != null)
        onContent("happy")
    } else if (et == EventType.SubElementList) {
      if (onList != null)
        onList(List("first", "second", "third"))
    }
  }

  def matches(node: Node): Boolean =
    node != null &&
      node.isInstanceOf[Element] &&
      !node.asInstanceOf[Element].isEndElement &&
      node.asInstanceOf[Element].name == targetElement &&
      (attributeRule == null || (attributeRule matches node.asInstanceOf[Element]))
}


/*
  def this(targetElement: String, onStartElement: (Element) => Unit) = this(targetElement, null, EventType.StartElement, Set(), onStartElement, null, null, null, null)

  def this(targetElement: String, attributeRule: AttributeRule, onStartElement: (Element) => Unit) = this(targetElement, attributeRule, EventType.StartElement, Set(), onStartElement, null, null, null, null)
  def this(targetElement: String, onStartElement: (Element) => Unit, subRules: Set[ElementRule]) = this(targetElement, null, EventType.StartElement, subRules, onStartElement, null, null, null, null)
  def this(targetElement: String, attributeRule: AttributeRule, subRules: Set[ElementRule]) = this(targetElement, attributeRule, EventType.StartElement, subRules, null, null, null, null, null)
  def this(targetElement: String, attributeRule: AttributeRule, onStartElement: (Element) => Unit, subRules: Set[ElementRule]) = this(targetElement, attributeRule, EventType.StartElement, subRules, onStartElement, null, null, null, null)

  // onStartElement and onEndElement constructors:
  def this(targetElement: String, onStartElement: (Element) => Unit, onEndElement: () => Unit) = this(targetElement, null, EventType.StartElement, Set(), onStartElement, onEndElement, null, null, null)
  def this(targetElement: String, attributeRule: AttributeRule, onStartElement: (Element) => Unit, onEndElement: () => Unit) = this(targetElement, attributeRule, EventType.StartElement, Set(), onStartElement, onEndElement, null, null, null)
  def this(targetElement: String, onStartElement: (Element) => Unit, onEndElement: () => Unit, subRules: Set[ElementRule]) = this(targetElement, null, EventType.StartElement, subRules, onStartElement, onEndElement, null, null, null)
  def this(targetElement: String, attributeRule: AttributeRule, onStartElement: (Element) => Unit, onEndElement: () => Unit, subRules: Set[ElementRule]) = this(targetElement, attributeRule, EventType.StartElement, subRules, onStartElement, onEndElement, null, null, null)

  // onText constructors:
  def this(targetElement: String, onText: (List[String]) => Unit) = this(targetElement, null, EventType.Text, Set(), null, null, onText, null, null)
  def this(targetElement: String, attributeRule: AttributeRule, onText: (List[String]) => Unit) = this(targetElement, attributeRule, EventType.Text, null, null, null, onText, null, null)


*/

object ElementRule {
  def startRule(targetElement: String, onStartElement: (Element) => Unit) = new ElementRule(targetElement, null, EventType.StartElement, Set(), onStartElement, null, null, null, null)
  def startRule(targetElement: String, attributeRule: AttributeRule, onStartElement: (Element) => Unit) = new ElementRule(targetElement, attributeRule, EventType.StartElement, Set(), onStartElement, null, null, null, null)
  def startRule(targetElement: String, subRules: Set[ElementRule]) = new ElementRule(targetElement, null, EventType.StartElement, subRules, null, null, null, null, null)
  def startRule(targetElement: String, onStartElement: (Element) => Unit, subRules: Set[ElementRule]) = new ElementRule(targetElement, null, EventType.StartElement, subRules, onStartElement, null, null, null, null)
  def startRule(targetElement: String, attributeRule: AttributeRule, subRules: Set[ElementRule]) = new ElementRule(targetElement, attributeRule, EventType.StartElement, subRules, null, null, null, null, null)
  def startRule(targetElement: String, attributeRule: AttributeRule, onStartElement: (Element) => Unit, subRules: Set[ElementRule]) = new ElementRule(targetElement, attributeRule, EventType.StartElement, subRules, onStartElement, null, null, null, null)

  // onStartElement and onEndElement constructors:
  def startRule(targetElement: String, onStartElement: (Element) => Unit, onEndElement: () => Unit) = new ElementRule(targetElement, null, EventType.StartElement, Set(), onStartElement, onEndElement, null, null, null)
  def startRule(targetElement: String, attributeRule: AttributeRule, onStartElement: (Element) => Unit, onEndElement: () => Unit) = new ElementRule(targetElement, attributeRule, EventType.StartElement, Set(), onStartElement, onEndElement, null, null, null)
  def startRule(targetElement: String, onStartElement: (Element) => Unit, onEndElement: () => Unit, subRules: Set[ElementRule]) = new ElementRule(targetElement, null, EventType.StartElement, subRules, onStartElement, onEndElement, null, null, null)
  def startRule(targetElement: String, attributeRule: AttributeRule, onStartElement: (Element) => Unit, onEndElement: () => Unit, subRules: Set[ElementRule]) = new ElementRule(targetElement, attributeRule, EventType.StartElement, subRules, onStartElement, onEndElement, null, null, null)

  // onText constructors:
  def textRule(targetElement: String, onText: (List[String]) => Unit) = new ElementRule(targetElement, null, EventType.Text, Set(), null, null, onText, null, null)
  def textRule(targetElement: String, attributeRule: AttributeRule, onText: (List[String]) => Unit) = new ElementRule(targetElement, attributeRule, EventType.Text, null, null, null, onText, null, null)
  def textRule(targetElement: String, onStartElement: (Element) => Unit, onText: (List[String]) => Unit) = new ElementRule(targetElement, null, EventType.Text, Set(), onStartElement, null, onText, null, null)

}