package com.rhyssoft.htmlparser.test

import java.io.FileReader
import com.rhyssoft.bookcompiler.TreeParser
import com.rhyssoft.htmlparser.{Element, Text}

object TreeParserRunner extends App {
  
  val elements = TreeParser.parseText(new FileReader("C:\\rtemp\\1. Chapter One\\1.ChapterOne.html"), onlyWithinBody = true)

  // The first paragraph is the Chapter number; the second is the Chapter Title; after that, everything is a normal paragraph:

  var indentation = -1

  elements.foreach{

    case e: Element =>

      if (e.isStartElement) {
        print(e)
        if (!e.empty) {
          indentation = indentation + 1
        }
      } else {
        indentation = indentation - 1
        print(e)
      }

    case t: Text =>
      print(t)
  }

  def indent(indentation: Int) = {
    val sb = new StringBuilder()

    for (i <- 0 to indentation) {
      sb.append("\t")
    }

    sb.toString()
  }
}
