/*
 * Main.scala
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.rhyssoft.htmlparser.test

import java.io.FileReader;
import java.util.ArrayList
import com.rhyssoft.htmlparser._

object Main {
  
    def test1(): Unit = {
      val rule = ElementRule.startRule (
        "body",
        (e: Element) => println("Found <BODY>"),
        Set(
          ElementRule.startRule(
            "div",
            new AttributeContainsRule(
                "class",
                "attr2value"
            ),
            (e: Element) => println("Found <DIV>"),
            Set(
              ElementRule.startRule(
                "span",
                (e: Element) => println("Found <SPAN>")
              ),
              ElementRule.startRule(
                "div",
                (e: Element) => println("Within second-level div!"),
                Set(
                  ElementRule.startRule(
                    "span",
                   (e: Element) => println("Within inner span!")
                  )
                )
              )
            )
          )
        )
      )

      val fr = new FileReader("src/com/rhyssoft/htmlparser/test/test1.html.txt")

      new Parser(rule, fr)
    }

    def testParseSmileStatement(): Unit = {

      val entries = new ArrayList[StatementEntry]
      var currentEntry: StatementEntry = null
      var thisCount = 0

      val rule = ElementRule.startRule (
        "table",
        new AttributeEqualsRule(
          "class",
          "summaryTable"
        ),
        Set(
          ElementRule.startRule(
            "tr",
            (e: Element) => { currentEntry = new StatementEntry; thisCount = 0 },
            () => { entries add currentEntry },
            Set(
              ElementRule.textRule(
                "td",
                (strings: List[String]) => {
                  val s = strings.mkString

                  if (thisCount == 0) {
                      currentEntry.date = s
                  } else if (thisCount == 1) {
                      currentEntry.payee = s
                  } else if (thisCount == 2) {
                      currentEntry.payer = s
                  } else if (thisCount == 3) {
                      currentEntry.amount = s
                  } else if (thisCount == 4) {
                      currentEntry.total = s
                  }

                  thisCount += 1
                }
              )
            )
          )
        )
      )

      val fr = new FileReader("src/com/rhyssoft/htmlparser/test/smile-statement.html.txt")
      new Parser(rule, fr)

      for (i <- 0 to entries.size - 1)
        println(entries.get(i))
    }

    def testGoogleFinance(): Unit = {

      val rule = ElementRule.startRule(
        "body",
        Set(
          ElementRule.startRule(
            "form",
            new AttributeEqualsRule("action", "/finance/historical"),
            Set(
              ElementRule.startRule(
                "input",
                new AttributeEqualsRule("name", "cid"),
                (e: Element) => {
                  val attributes = e.attributes
                  println("cid = " + attributes("value"))
                }
              )
            )
          ),
          ElementRule.startRule(
            "table",
            new AttributeEqualsRule("id", "historical_price"),
            (e: Element) => println("*** Got prices TABLE ***"),
            Set(
              ElementRule.startRule(
                "tr",
                (e: Element) => println("<tr>"),
                () => println("</tr>")
              )
            )
          )
        )
      )

      val fr = new FileReader("src/com/rhyssoft/htmlparser/test/google-finance.html.txt")
      new Parser(rule, fr)
      fr.close()
    }

    private def testMissingEndTags() {

      var rows = List[List[Int]]()
      var currentRow: List[Int] = null

      val rule = ElementRule.startRule(
        "body",
        Set(
          ElementRule.startRule(
            "table",
            Set(
              ElementRule.startRule(
                "tr",
                (e: Element) => currentRow = List(),
                () => rows = rows ::: List(currentRow),
                Set(
                  ElementRule.textRule(
                    "td",
                    (strings: List[String]) => currentRow = currentRow ::: List(Integer.parseInt(strings.mkString.trim))
                  )
                )
              )
            )
          )
        )
      )

      val fr = new FileReader("src/com/rhyssoft/htmlparser/test/missing-empty-tags.html")
      new Parser(rule, fr)
      fr.close()

      for (row <- rows) {
        for (i <- row) {
          print(String.format("%4d", int2Integer(i)))
        }
        println()
      }
  }

    /**
     * @param args the command line arguments
     */
    def main(args: Array[String]): Unit = {
//      test1()
//      testParseSmileStatement()
//      testMissingEndTags()
//      testGoogleFinance()

    }
}
