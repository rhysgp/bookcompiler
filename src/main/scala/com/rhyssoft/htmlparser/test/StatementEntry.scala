/*
 * StatementEntry.scala
 *
 * Copyright (c) 2009 Rhys Parsons. All rights reserved.
 * Use of this code is strictly prohibited without written permission from the
 * author.
 */

package com.rhyssoft.htmlparser.test

class StatementEntry {
  var date: String = ""
  var payee: String = ""
  var payer: String = ""
  var amount: String = ""
  var total: String = ""

  override def toString(): String = String.format("%-15s %-30s %10s %10s %10s", 
                                                  date.trim,
                                                  payee.trim,
                                                  payer.trim,
                                                  amount.trim,
                                                  total.trim)
}
