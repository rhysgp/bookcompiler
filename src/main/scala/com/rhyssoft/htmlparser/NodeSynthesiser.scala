/*
 * ElementParser.scala
 *
 * Copyright (c) 2009 Rhys Parsons. All rights reserved.
 * Use of this code is strictly prohibited without written permission from the
 * author.
 */

package com.rhyssoft.htmlparser

abstract class NodeSynthesiser {
  protected def synthesiseMissingNodes(node: Node): List[Node]
}
