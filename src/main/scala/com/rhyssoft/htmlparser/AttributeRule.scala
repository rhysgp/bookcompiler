/*
 * AttributeRule.scala
 *
 * Copyright (c) 2009 Rhys Parsons. All rights reserved.
 * Use of this code is strictly prohibited without written permission from the
 * author.
 */

package com.rhyssoft.htmlparser

class AttributeRule(val name: String, val value: String, val matchFunction: (Element) => Boolean)  {

  def matches(element: Element): Boolean = matchFunction(element)
}
