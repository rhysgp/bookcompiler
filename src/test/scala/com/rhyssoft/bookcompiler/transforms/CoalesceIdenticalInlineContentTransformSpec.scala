package com.rhyssoft.bookcompiler.transforms

import com.rhyssoft.bookcompiler.model.InlineContent
import org.specs2.mutable.Specification

class CoalesceIdenticalInlineContentTransformSpec extends Specification {

  "coalescing identical inline content elements" should {

    "coalesce two identical ICs that have no style" in {
      val input = List(InlineContent.txt(content = "Mary had a little lamb, "), InlineContent.txt(content = "It's fleece was white as snow."))
      val result = CoalesceIdenticalInlineContentTransform.process(input)
      result mustEqual List(InlineContent.txt(content = "Mary had a little lamb, It's fleece was white as snow."))
    }

    "coalesce two identical ICs that have the same style" in {
      val input = List(InlineContent.txt(content = "Mary had a little lamb, ", style = "chic"), InlineContent.txt(content = "It's fleece was white as snow.", style = "chic"))
      val result = CoalesceIdenticalInlineContentTransform.process(input)
      result mustEqual List(InlineContent.txt(content = "Mary had a little lamb, It's fleece was white as snow.", style = "chic"))
    }

    "coalesce three identical ICs that have the same style" in {
      val input = List(InlineContent.txt(content = "Mary had a little lamb, ", style = "chic"), InlineContent.txt(content = "It's fleece was white as snow.", style = "chic"), InlineContent.txt(content = "And everywhere that Mary went.", style = "chic"))
      val result = CoalesceIdenticalInlineContentTransform.process(input)
      result mustEqual List(InlineContent.txt(content = "Mary had a little lamb, It's fleece was white as snow.And everywhere that Mary went.", style = "chic"))
    }

    "only coalesce those with the same style" in {
      val input = List(
        InlineContent.txt(content = "Here's a silly poem:", style = "prelim"),
        InlineContent.txt(content = "Mary had a little lamb, ", style = "chic"),
        InlineContent.txt(content = "It's fleece was white as snow.", style = "chic"),
        InlineContent.txt(content = "And everywhere that Mary went,", style = "rough"),
        InlineContent.txt(content = "The lamb was sure to go.", style = "rough"),
        InlineContent.txt(content = "------------------------------", style = "end")
      )
      val result = CoalesceIdenticalInlineContentTransform.process(input)
      result mustEqual List(
        InlineContent.txt(content = "Here's a silly poem:", style = "prelim"),
        InlineContent.txt(content = "Mary had a little lamb, It's fleece was white as snow.", style = "chic"),
        InlineContent.txt(content = "And everywhere that Mary went,The lamb was sure to go.", style = "rough"),
        InlineContent.txt(content = "------------------------------", style = "end")
      )
    }
  }

}
