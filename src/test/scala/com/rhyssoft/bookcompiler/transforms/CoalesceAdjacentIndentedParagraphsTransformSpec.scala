package com.rhyssoft.bookcompiler.transforms

import com.rhyssoft.bookcompiler.model.{Chapter, InlineContent, Paragraph, ParagraphStyle}
import org.specs2.mutable.Specification

class CoalesceAdjacentIndentedParagraphsTransformSpec  extends Specification {

  val transform = new CoalesceAdjacentIndentedParagraphsTransform()

  "coalescing adjacent indented paragraphs" should {
    "merge adjacent indented paragraphs" in {

      val l1 = "'Twas brillig, and the slithy toves"
      val l2 = "Did gyre and gimble in the wabe"
      val p1 = Paragraph(List(InlineContent.txt(l1)), ParagraphStyle.Indented)
      val p2 = Paragraph(List(InlineContent.txt(l2)), ParagraphStyle.Indented)
      val chapter = Chapter(p1 :: p2 :: Nil)

      val transformedChapter = transform.process(chapter)
      transformedChapter.paragraphs must haveLength(1)
      transformedChapter.paragraphs.head.inlineContent must haveLength(3)
      transformedChapter.paragraphs.head.inlineContent.head mustEqual InlineContent.txt(l1)
      transformedChapter.paragraphs.head.inlineContent(1) mustEqual InlineContent.br
      transformedChapter.paragraphs.head.inlineContent(2) mustEqual InlineContent.txt(l2)
    }
  }

  "leave adjacent non-indented paragraphs in tact" in {

    val p0 = Paragraph(List(InlineContent.txt("Here is a silly poem:")), ParagraphStyle.Normal)
    val l1 = "'Twas brillig, and the slithy toves"
    val l2 = "Did gyre and gimble in the wabe"
    val p1 = Paragraph(List(InlineContent.txt(l1)), ParagraphStyle.Indented)
    val p2 = Paragraph(List(InlineContent.txt(l2)), ParagraphStyle.Indented)
    val p3 = Paragraph(List(InlineContent.txt("Okay, that's enough of that")), ParagraphStyle.Normal)
    val chapter = Chapter(p0 :: p1 :: p2 :: p3 :: Nil)

    val transformedChapter = transform.process(chapter)
    transformedChapter.paragraphs must haveLength(3)
    transformedChapter.paragraphs.head mustEqual p0
    transformedChapter.paragraphs(1).inlineContent must haveLength(3)
    transformedChapter.paragraphs(1).inlineContent.head mustEqual InlineContent.txt(l1)
    transformedChapter.paragraphs(1).inlineContent(1) mustEqual InlineContent.br
    transformedChapter.paragraphs(1).inlineContent(2) mustEqual InlineContent.txt(l2)
    transformedChapter.paragraphs(2) mustEqual p3
  }

  "remove empty lines either side of the group" in {

    val p0 = Paragraph(List(InlineContent.txt("Here is a silly poem:")), ParagraphStyle.Normal)
    val ic1 = InlineContent.txt("'Twas brillig, and the slithy toves")
    val ic2 = InlineContent.txt("Did gyre and gimble in the wabe")
    val p1 = Paragraph(List(InlineContent.txt("")), ParagraphStyle.Indented)
    val p2 = Paragraph(List(ic1), ParagraphStyle.Indented)
    val p3 = Paragraph(List(ic2), ParagraphStyle.Indented)
    val p4 = Paragraph(List(InlineContent.txt("")), ParagraphStyle.Indented)
    val p5 = Paragraph(List(InlineContent.txt("Okay, that's enough of that")), ParagraphStyle.Normal)
    val chapter = Chapter(p0 :: p1 :: p2 :: p3 :: p4 :: p5 :: Nil)

    val transformedChapter = transform.process(chapter)
    transformedChapter.paragraphs must haveLength(3)
    transformedChapter.paragraphs.head mustEqual p0
    transformedChapter.paragraphs(1).inlineContent must haveLength(3)
    transformedChapter.paragraphs(1).inlineContent.head mustEqual ic1
    transformedChapter.paragraphs(1).inlineContent(1) mustEqual InlineContent.br
    transformedChapter.paragraphs(1).inlineContent(2) mustEqual ic2
    transformedChapter.paragraphs(2) mustEqual p5
  }
}
