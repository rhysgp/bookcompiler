package com.rhyssoft.bookcompiler.transforms

import com.rhyssoft.bookcompiler.model.{InlineContent, Paragraph, ParagraphStyle}
import org.specs2.mutable.Specification

class FixupEmptyParagraphsInlineContentTransformSpec extends Specification {

  "fixing up empty paragraphs" should {
    "work for spaces" in {
      val inPara = Paragraph.txt("     ")
      val outPara = FixupEmptyParagraphsInlineContentTransform.process(inPara)
      outPara.inlineContent must haveLength(0)
    }

    "work for spaces and tabs" in {
      val inPara = Paragraph.txt(" \t  \t \t ")
      val outPara = FixupEmptyParagraphsInlineContentTransform.process(inPara)
      outPara.inlineContent must haveLength(0)
    }

    "work for actual empty text" in {
      val inPara = Paragraph.txt("")
      val outPara = FixupEmptyParagraphsInlineContentTransform.process(inPara)
      outPara.inlineContent must haveLength(0)
    }

    "not work for new lines" in {
      val inPara = Paragraph("", List(InlineContent.br), ParagraphStyle.Normal)
      val outPara = FixupEmptyParagraphsInlineContentTransform.process(inPara)
      outPara mustEqual inPara
    }
  }

}
