package com.rhyssoft.bookcompiler.transforms

import java.nio.file.Files

import com.rhyssoft.IOUtils
import com.rhyssoft.bookcompiler.model.{Chapter, InlineContent, Paragraph, ParagraphStyle}
import org.specs2.mutable.Specification

class ImgProcessorSpec extends Specification {

  val dir = Files.createTempDirectory("compile.test.")
  val processor = new ImgProcessor(dir, includeFileProtocol = true)

  println(dir)

  Runtime.getRuntime.addShutdownHook(new Thread() { override def run() { IOUtils.rmDir(dir) } })

  "img processing" should {

    val imgUrl = getClass.getClassLoader.getResource("img-processor/bun.png")
    val chapter = Chapter(Paragraph(InlineContent.img(imgUrl.toString, alt = "bun.png") :: Nil, ParagraphStyle.Normal) :: Nil)

    val chapterWithImage = processor.process(chapter)

    "download the img" in {
      Files.exists(dir.resolve("images/0_bun.png")) must beTrue
    }

    "replace the src with the local file" in {
      val ic = chapterWithImage.paragraphs.head.inlineContent.head
      ic.content mustEqual "images/0_bun.png"
    }
  }
}
