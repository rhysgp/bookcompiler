package com.rhyssoft.bookcompiler.transforms

import com.rhyssoft.bookcompiler.model.{Chapter, Paragraph}
import org.specs2.mutable.Specification

class InterParaSeparatorsProcessorSpec extends Specification {

  val processor = new InterParaSeparatorsProcessor()
  val separatorParas = Paragraph.empty :: Paragraph.txt("*   *   *") :: Paragraph.empty :: Nil

  "inter para separator processor" should {
    "find and replace an empty para followed by three asterisks followed by an empty para with a single para with the 'separator' style" in {
      val inChapter = Chapter(separatorParas)
      val outChapter = processor.process(inChapter)
      outChapter.paragraphs must haveLength(1)
      outChapter.paragraphs.head mustEqual Paragraph.separator
    }

    "and leave other paras intact" in  {
      val p1 = Paragraph.txt("prev para")
      val p2 = Paragraph.txt("subs para")
      val paras = (p1 :: separatorParas) :+ p2
      val inChapter = Chapter(paras)
      val outChapter = processor.process(inChapter)
      outChapter.paragraphs must haveLength(3)
      outChapter.paragraphs mustEqual p1 :: Paragraph.separator :: p2 :: Nil
    }
  }

}
