package com.rhyssoft.bookcompiler.transforms

import java.io.StringReader

import com.rhyssoft.bookcompiler.model.{Chapter, InlineContent, Paragraph, ParagraphStyle}
import org.specs2.mutable.Specification

class RationaliseTextStylesTransformSpec extends Specification {

  val transform = RationaliseTextStylesTransform

  "rationalising paragraph style" should {
    "remove default margin-left" in {
      val chapter = Chapter(List(Paragraph("margin-left: 10px", Nil)))
      val actual = transform.process(chapter)
      val expected = chapter
      actual mustEqual expected
    }

    "keep non-default margin-left" in {
      val chapter = Chapter(List(Paragraph("margin-left: 10px", Nil), Paragraph("margin-left: 15px", Nil), Paragraph("margin-left: 10px", Nil)))
      val actual = transform.process(chapter)
      val expectedIndents = List(false, true, false)
      actual.paragraphs.map(_.style == ParagraphStyle.Indented) mustEqual expectedIndents
    }

    "keep margin-left when default is to have none" in {
      val chapter = Chapter(List(Paragraph("", Nil), Paragraph("margin-left:15pt", Nil), Paragraph("", Nil)))
      val actual = transform.process(chapter)
      val expectedIndents = List(false, true, false)
      actual.paragraphs.map(_.style == ParagraphStyle.Indented) mustEqual expectedIndents
    }

    "continuation when normal is text-indent" in  {
      val chapter = Chapter(List(Paragraph("text-indent:20pt", Nil), Paragraph("text-indent:20pt", Nil), Paragraph("text-indent:20pt", Nil), Paragraph("margin-left: 15pt", Nil), Paragraph("", Nil)))
      val actual = transform.process(chapter)
      val expectedContinuation = List(ParagraphStyle.Normal, ParagraphStyle.Normal, ParagraphStyle.Normal, ParagraphStyle.Indented, ParagraphStyle.Continuation)
      actual.paragraphs.map(_.style) mustEqual expectedContinuation
    }

    "continuation paragraphs should only follow indented" in {
      val chapter = Chapter(List(Paragraph("text-indent:20pt", Nil), Paragraph("text-indent:20pt", Nil), Paragraph("text-indent:20pt", Nil), Paragraph("", Nil)))
      val actual = transform.process(chapter)
      val expectedContinuation = List(ParagraphStyle.Normal, ParagraphStyle.Normal, ParagraphStyle.Normal, ParagraphStyle.Normal)
      actual.paragraphs.map(_.style) mustEqual expectedContinuation
    }

    "process 'bun' paragraph" in {
      val text =
        """
          |<html>
          |<body>
          |<p style="padding:0;margin:0;color:#000000;text-indent:34.5pt;font-size:11pt;font-family:&quot;Arial&quot;;line-height:1.15;orphans:2;widows:2;text-align:left"><span style="color:#000000;font-weight:400;text-decoration:none;vertical-align:baseline;font-size:11pt;font-family:&quot;Arial&quot;;font-style:normal">A look of puzzlement and concern appeared on Tetu&rsquo;s face. &ldquo;That cannot be right,&rdquo; he said. He gently took the brush from Ailsa and crossed out the vowels. &ldquo;If these were Egyptian hieroglyphs, we would not write these sounds.&rdquo; Ailsa pulled a face that said, &ldquo;OK, but that&rsquo;s just silly,&rdquo; which Tetu could not see. Underneath, Tetu wrote:</span></p>
          |<p style="padding-top:10pt;margin:0;color:#000000;padding-left:0;font-size:11pt;padding-bottom:10pt;font-family:&quot;Arial&quot;;line-height:1.15;orphans:2;widows:2;text-align:center;padding-right:0"><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 21.00px; height: 12.00px;"><img alt="bun2.png" src="https://lh5.googleusercontent.com/WzStcOJynIxgtn-OyWeNgS8p6PK_OoKXD_kly38gFk0glhpT31ZJxvHsMumTb5c0O2Xw1J5jv-8iLFGdVcHhrZZfbgZ65_GqWMR9QTQRfJHhi6zFVxOJLpyBttrBLYlK82dyguhg" style="width: 21.00px; height: 12.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 21.00px; height: 12.00px;"><img alt="bun2.png" src="https://lh5.googleusercontent.com/WzStcOJynIxgtn-OyWeNgS8p6PK_OoKXD_kly38gFk0glhpT31ZJxvHsMumTb5c0O2Xw1J5jv-8iLFGdVcHhrZZfbgZ65_GqWMR9QTQRfJHhi6zFVxOJLpyBttrBLYlK82dyguhg" style="width: 21.00px; height: 12.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span></p>
          |<p style="padding:0;margin:0;color:#000000;font-size:11pt;font-family:&quot;Arial&quot;;line-height:1.15;orphans:2;widows:2;text-align:left"><span style="color:#000000;font-weight:400;text-decoration:none;vertical-align:baseline;font-size:11pt;font-family:&quot;Arial&quot;;font-style:normal">saying, &ldquo;This is how my name is written.&rdquo;</span></p>
          |</body>
          |</html>
        """.stripMargin.replace("\n", "")

      val chapter = GoogleDocHtmlToInternalTransform.process(new StringReader(text))
      val rationalisedChapter = RationaliseTextStylesTransform.process(chapter)

      rationalisedChapter.paragraphs(1).styleClass mustEqual Set("bunImages", "centre")

    }

  }

  def txt(c: String = "", s: String = "") = InlineContent.txt(c, s)
}
