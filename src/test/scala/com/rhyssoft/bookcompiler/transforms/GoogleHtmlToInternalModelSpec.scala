package com.rhyssoft.bookcompiler.transforms

import java.nio.file.Files

import com.rhyssoft.IOUtils._
import com.rhyssoft.bookcompiler.ChapterFile
import com.rhyssoft.bookcompiler.model.ParagraphStyle
import com.rhyssoft.rewriter.GoogleHtmlToInternalModel
import org.specs2.mutable.Specification

class GoogleHtmlToInternalModelSpec extends Specification {
  "Converting Google HTML to internal format" should {

    "convert section breaks correctly" in {

      val input =
      """
        |<html>
        |<body>
        |<p></p>
        |<p></p>
        |<p style="padding:0;margin:0;color:#fff2cc;text-indent:12.7pt;font-size:11pt;font-family:&quot;Arial&quot;;line-height:1.15;orphans:2;widows:2;text-align:left"><span style="color:#fff2cc;font-weight:400;text-decoration:none;vertical-align:baseline;font-size:11pt;font-family:&quot;Arial&quot;;font-style:normal">It was a hot day, the air motionless. Sitting, Ailsa soon became sleepy. Her head jerked as she fell asleep, and woke her; until, asleep already, she lay along the bench, pillowing her head with her arm.</span></p>
        |<p style="padding:0;margin:0;color:#fff2cc;font-size:11pt;font-family:&quot;Arial&quot;;line-height:1.15;orphans:2;widows:2;height:11pt;text-align:left"><span style="color:#fff2cc;font-weight:400;text-decoration:none;vertical-align:baseline;font-size:11pt;font-family:&quot;Arial&quot;;font-style:normal"></span></p>
        |<p style="padding:0;margin:0;color:#fff2cc;font-size:11pt;font-family:&quot;Arial&quot;;line-height:1.15;orphans:2;widows:2;text-align:center"><span style="color:#fff2cc;font-weight:400;text-decoration:none;vertical-align:baseline;font-size:11pt;font-family:&quot;Arial&quot;;font-style:normal">*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*</span></p>
        |<p style="padding:0;margin:0;color:#fff2cc;font-size:11pt;font-family:&quot;Arial&quot;;line-height:1.15;orphans:2;widows:2;height:11pt;text-align:left"><span style="color:#fff2cc;font-weight:400;text-decoration:none;vertical-align:baseline;font-size:11pt;font-family:&quot;Arial&quot;;font-style:normal"></span></p>
        |<p style="padding:0;margin:0;color:#fff2cc;text-indent:12.7pt;font-size:11pt;font-family:&quot;Arial&quot;;line-height:1.15;orphans:2;widows:2;text-align:left"><span>As Ailsa fell asleep, deep under the Ashmolean an ancient intelligence was waking up. Dreaming still of astrolabes and papyrus scrolls with antique writing of antique languages, his eyes began to flutter, and his breath grew deeper. Profound thoughts he left in the depths, ascending slowly towards consciousness, his face twitching and grimacing at the changes in pressure, whilst in the base of his mind a thought bubbled, containing one word: </span><span style="font-style:italic">why</span><span style="color:#fff2cc;font-weight:400;text-decoration:none;vertical-align:baseline;font-size:11pt;font-family:&quot;Arial&quot;;font-style:normal">?</span></p>
        |</body>
        |</html>
      """.stripMargin.replace("\n", "")

      val tempFile = Files.createTempFile("GoogleHtmlToInternalModelSpec_", ".html")
      writeToFile(input, tempFile)
      val chapterFile = ChapterFile("Test chapter", tempFile)

      val chapter = GoogleHtmlToInternalModel.convertGoogleHtmlToInternal(chapterFile, 1)

      chapter.paragraphs.map(_.style) mustEqual Seq(ParagraphStyle.Normal, ParagraphStyle.Separator, ParagraphStyle.Normal)
    }

  }
}
