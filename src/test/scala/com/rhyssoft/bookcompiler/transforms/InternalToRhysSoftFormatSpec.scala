package com.rhyssoft.bookcompiler.transforms

import com.rhyssoft.bookcompiler.model._
import org.specs2.mutable.Specification


class InternalToRhysSoftFormatSpec extends Specification {

  "converting to rhyssoft format" should {

    val t = "Underground, overground, wombling free"

    "produce correct metadata" in {
      val title = "test title"
      val chapterNum = 32
      val chapterType = ChapterType.Chapter
      val sb = new StringBuilder()
      InternalToRhysSoftFormat.outputMetadata(ChapterMetadata(title, chapterNum, chapterType), sb)
      sb.toString mustEqual s"{chapter number:$chapterNum title:§$title§ type:$chapterType}\n"
    }

    "produce correct style for normal paragraphs" in {
      val sb = new StringBuilder()
      val p = Paragraph("", List(InlineContent.txt(t)), ParagraphStyle.Normal)

      InternalToRhysSoftFormat.outputPara(p, sb)

      sb.toString mustEqual s"{p}$t\n"
    }

    "produce correct style for indented paragraphs" in {
      val sb = new StringBuilder()
      val p = Paragraph("", List(InlineContent.txt(t)), ParagraphStyle.Indented)

      InternalToRhysSoftFormat.outputPara(p, sb)

      sb.toString mustEqual s"{p:quote}$t\n"
    }

    "produce correct style for continuation paragraphs" in {
      val sb = new StringBuilder()
      val p = Paragraph("", List(InlineContent.txt(t)), ParagraphStyle.Continuation)

      InternalToRhysSoftFormat.outputPara(p, sb)

      sb.toString mustEqual s"{p:cont}$t\n"
    }

    "produce correct style for separators paragraphs" in {
      val sb = new StringBuilder()
      val p = Paragraph("", List(InlineContent.txt(t)), ParagraphStyle.Separator)

      InternalToRhysSoftFormat.outputPara(p, sb)

      sb.toString mustEqual s"{p:sep}$t\n"
    }

    "produce inline style for italics" in {
      val sb = new StringBuilder()
      val ic = InlineContent.txt(t, style = "font-style:italic")

      InternalToRhysSoftFormat.outputInlineContent(ic, sb)

      sb.toString mustEqual s"{ic:italic}$t{ic}"
    }

    "produce inline style for bold" in {
      val sb = new StringBuilder()
      val ic = InlineContent.txt(t, style = "font-weight:bold")

      InternalToRhysSoftFormat.outputInlineContent(ic, sb)

      sb.toString mustEqual s"{ic:bold}$t{ic}"
    }

    "produce inline style for font size" in {
      val sb = new StringBuilder()
      val ic = InlineContent.txt(t, style = "font-size:24pt")

      InternalToRhysSoftFormat.outputInlineContent(ic, sb)

      sb.toString mustEqual s"{ic:fs24pt}$t{ic}"
    }
  }

}