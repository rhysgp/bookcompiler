package com.rhyssoft.bookcompiler.transforms

import com.rhyssoft.bookcompiler.model.{Paragraph, _}
import org.specs2.mutable.Specification


class RhysSoftFormatToInternalModelSpec extends Specification {

  val transformer = RhysSoftFormatToInternalModel

  private val chapterHeader = "{chapter number:2 title:§foo§ type:Chapter}"
  val paraText1 = "It wasn't clear to him whether 'right' was an expression of relative direction, an indication of correctness, or a exclamatory pause."
  val paraText2 = "He decided he would ask her."
  val paraText3 = "'Excuse me, what did you mean?'"

  "Converting from RhysSoft chapter to internal model" should {

    "read chapter metadata correctly" in {
      val title = "This is my title"
      val t = ChapterType.Foreword
      val chapNum = 43
      val testInput = s"{chapter number:$chapNum title:§$title§ type:$t }"
      val stream = java.util.stream.Stream.of(testInput, "")

      transformer.process(stream) mustEqual Chapter(ChapterMetadata(title, chapNum, t), Nil)
    }

    "read a single normal paragraph correctly" in {
      val stream  = java.util.stream.Stream.of(chapterHeader, s"{p}$paraText1")

      val chapter = transformer.process(stream)

      chapter.paragraphs.length aka "number of paragraphs" mustEqual 1
      chapter.paragraphs.head mustEqual Paragraph(List(InlineContent.txt(paraText1)), ParagraphStyle.Normal)
    }

    "read multiple normal paragraphs correctly" in {
      val stream  = java.util.stream.Stream.of(chapterHeader, s"{p}$paraText1", s"{p}$paraText2", s"{p}$paraText3")

      val chapter = transformer.process(stream)

      chapter.paragraphs.length aka "number of paragraphs" mustEqual 3
      chapter.paragraphs mustEqual Seq(
        Paragraph(List(InlineContent.txt(paraText1)), ParagraphStyle.Normal),
        Paragraph(List(InlineContent.txt(paraText2)), ParagraphStyle.Normal),
        Paragraph(List(InlineContent.txt(paraText3)), ParagraphStyle.Normal)
      )
    }

    "read continuation paragraph" in {
      val stream  = java.util.stream.Stream.of(chapterHeader, s"{p:cont}$paraText1")

      val chapter = transformer.process(stream)

      chapter.paragraphs.length aka "number of paragraphs" mustEqual 1
      chapter.paragraphs.head mustEqual Paragraph(List(InlineContent.txt(paraText1)), ParagraphStyle.Continuation)
    }

    "read indented" in {
      val stream  = java.util.stream.Stream.of(chapterHeader, s"{p:quot}$paraText1")

      val chapter = transformer.process(stream)

      chapter.paragraphs.length aka "number of paragraphs" mustEqual 1
      chapter.paragraphs.head mustEqual Paragraph(List(InlineContent.txt(paraText1)), ParagraphStyle.Indented)
    }

    "read separator" in {
      val stream  = java.util.stream.Stream.of(chapterHeader, s"{p:sep}$paraText1")

      val chapter = transformer.process(stream)

      chapter.paragraphs.length aka "number of paragraphs" mustEqual 1
      chapter.paragraphs.head mustEqual Paragraph(List(InlineContent.txt(paraText1)), ParagraphStyle.Separator)
    }

    "read italic inline content styles" in {
      val c = s"{p}This should use {ic:italic}italics{ic}!"
      val stream = java.util.stream.Stream.of(chapterHeader, c)

      val chapter = transformer.process(stream)

      chapter.paragraphs.length aka "number of paragraphs" mustEqual 1
      val para = chapter.paragraphs.head
      para.inlineContent mustEqual Seq(
        InlineContent.txt("This should use "),
        InlineContent.txt("italics", style="italic"),
        InlineContent.txt("!")
      )
    }

    "read bold inline content styles" in {
      val c = s"{p}This should use {ic:bold}bold{ic}!"
      val stream = java.util.stream.Stream.of(chapterHeader, c)

      val chapter = transformer.process(stream)

      chapter.paragraphs.length aka "number of paragraphs" mustEqual 1
      val para = chapter.paragraphs.head
      para.inlineContent mustEqual Seq(
        InlineContent.txt("This should use "),
        InlineContent.txt("bold", style="bold"),
        InlineContent.txt("!")
      )
    }

    "read font-size inline content styles" in {
      val c = s"{p}This should use {ic:fs24pt}large text{ic}!"
      val stream = java.util.stream.Stream.of(chapterHeader, c)

      val chapter = transformer.process(stream)

      chapter.paragraphs.length aka "number of paragraphs" mustEqual 1
      val para = chapter.paragraphs.head
      para.inlineContent mustEqual Seq(
        InlineContent.txt("This should use "),
        InlineContent.txt("large text", style="fs24pt"),
        InlineContent.txt("!")
      )
    }

    "ignore lines starting with hash" in {
      val c = s"{p}This should use {ic:fs24pt}large text{ic}!"
      val stream = java.util.stream.Stream.of(
        chapterHeader,
        "# This is a comment that shouldn't be rendered",
        c
      )

      val chapter = transformer.process(stream)

      chapter.paragraphs.length aka "number of paragraphs" mustEqual 1
      val para = chapter.paragraphs.head
      para.inlineContent mustEqual Seq(
        InlineContent.txt("This should use "),
        InlineContent.txt("large text", style="fs24pt"),
        InlineContent.txt("!")
      )
    }

    "read line breaks correctly" in {
      val c = s"{p}Here is a paragraph{br}separated!"
      val stream = java.util.stream.Stream.of(chapterHeader, c)
      val chapter = transformer.process(stream)
      val para = chapter.paragraphs.head
      para.inlineContent mustEqual Seq(
        InlineContent.txt(content = "Here is a paragraph"),
        InlineContent.br,
        InlineContent.txt(content = "separated!")
      )
    }

    "read multiple line breaks and inline styles correctly" in {
      val c = s"{p}Here is a {ic:italic}paragraph{ic}{br}separated{ic:bold}!{ic}"
      val stream = java.util.stream.Stream.of(chapterHeader, c)
      val chapter = transformer.process(stream)
      val para = chapter.paragraphs.head
      para.inlineContent mustEqual Seq(
        InlineContent.txt(content = "Here is a "),
        InlineContent.txt(content = "paragraph", style = "italic"),
        InlineContent.br,
        InlineContent.txt(content = "separated"),
        InlineContent.txt(content = "!", style = "bold")
      )
    }
  }

  "parsing images" should {
    "Create InlineContent images with no style" in {
      transformer.parseIcImg("{img:images/blue.png}") mustEqual InlineContent("images/blue.png", "", "", ContentType.Image)
    }
    "Create InlineContent images with a style" in {
      transformer.parseIcImg("{img:images/blue.png:flashy}") mustEqual InlineContent("images/blue.png", "flashy", "", ContentType.Image)
    }
  }
}
