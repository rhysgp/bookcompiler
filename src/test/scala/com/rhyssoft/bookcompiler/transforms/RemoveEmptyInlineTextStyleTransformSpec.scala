package com.rhyssoft.bookcompiler.transforms

import com.rhyssoft.bookcompiler.model.{Chapter, InlineContent, Paragraph, ParagraphStyle}
import org.specs2.mutable.Specification

class RemoveEmptyInlineTextStyleTransformSpec extends Specification {

  "Removing empty inline styles" should {
    "remove empty inline styles" in {
      val inChapter = Chapter(List(Paragraph(List(InlineContent.txt("")), ParagraphStyle.Normal)))
      val outChapter = RemoveEmptyInlineTextStyleTransform.process(inChapter)
      outChapter mustEqual Chapter(List(Paragraph(Nil, ParagraphStyle.Normal)))
    }

    "remove empty inline styles when separated by non-empty styles" in {
      val inChapter = Chapter(List(Paragraph(List(InlineContent.txt("Foo"), InlineContent.txt(""), InlineContent.txt("Bar")), ParagraphStyle.Normal)))
      val outChapter = RemoveEmptyInlineTextStyleTransform.process(inChapter)
      outChapter mustEqual Chapter(List(Paragraph(List(InlineContent.txt("Foo"), InlineContent.txt("Bar")), ParagraphStyle.Normal)))
    }

    "not remove only whitespace content" in {
      val inChapter = Chapter(List(Paragraph(List(InlineContent.txt("  ")), ParagraphStyle.Normal)))
      val outChapter = RemoveEmptyInlineTextStyleTransform.process(inChapter)
      outChapter mustEqual inChapter
    }

    "not remove empty br" in {
      val inChapter = Chapter(List(Paragraph(List(InlineContent.br), ParagraphStyle.Normal)))
      val outChapter = RemoveEmptyInlineTextStyleTransform.process(inChapter)
      outChapter mustEqual inChapter
    }

    "not remove empty img" in {
      val inChapter = Chapter(List(Paragraph(List(InlineContent.img("")), ParagraphStyle.Normal)))
      val outChapter = RemoveEmptyInlineTextStyleTransform.process(inChapter)
      outChapter mustEqual inChapter
    }
  }



}
