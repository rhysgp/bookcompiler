package com.rhyssoft.bookcompiler.transforms

import com.rhyssoft.bookcompiler.model.{ContentType, InlineContent}
import com.rhyssoft.htmlparser.{Element, Text}
import org.specs2.mutable.Specification

class GoogleDocHtmlToInternalTransformSpec extends Specification {


  val transform = GoogleDocHtmlToInternalTransform


  "grouping into paragraphs" should {
    "group by <p>" in {

      val p1 = Element.start("p") :: Element.start("span") :: Text("text1") :: Element.end("span") :: Element.start("span") :: Text("text2") :: Element.end("span") :: Element.end("p") :: Nil
      val p2 = Element.start("p") :: Element.start("span") :: Text("text3") :: Element.end("span") :: Element.start("span") :: Text("text4") :: Element.end("span") :: Element.end("p") :: Nil

      val nodes = p1 ::: p2

      val grouped = transform.groupIntoParas(nodes)

      grouped mustEqual p1 :: p2 :: Nil
    }

    "group by h[1-6] too" in {

      val p1 = Element.start("h6") :: Element.start("span") :: Text("text1") :: Element.end("span") :: Element.start("span") :: Text("text2") :: Element.end("span") :: Element.end("h6") :: Nil
      val p2 = Element.start("h6") :: Element.start("span") :: Text("text3") :: Element.end("span") :: Element.start("span") :: Text("text4") :: Element.end("span") :: Element.end("h6") :: Nil

      val nodes = p1 ::: p2

      val grouped = transform.groupIntoParas(nodes)

      grouped mustEqual p1 :: p2 :: Nil
    }
  }

  "converting to para" should {
    "correctly interpret start spans" in {

      val spans = Element.start("span") :: Nil

      val para = transform.convertToPara(spans)

      para.inlineContent must haveLength(1)
      para.inlineContent.head mustEqual InlineContent("", "", "", ContentType.Text)
    }

    "correctly assign text to span-created inline-content" in {
      val spans = Element.start("span") :: Text("Alle Menschen werden Brüder") :: Element.end("span") :: Nil

      val para = transform.convertToPara(spans)

      para.inlineContent must haveLength(1)
      para.inlineContent.head mustEqual InlineContent("Alle Menschen werden Brüder", "", "", ContentType.Text)
    }

    "correctly pick up style data" in {
      val spans =
        Element.start("span", """style="font-weight: bold"""") :: Text("Alle Menschen werden Brüder") :: Element.end("span") :: Nil

      val para = transform.convertToPara(spans)

      para.inlineContent must haveLength(1)
      para.inlineContent.head mustEqual InlineContent("Alle Menschen werden Brüder", "font-weight: bold", "", ContentType.Text)
    }

    "ignore things out of place" in  {
      val spans =
        Element.start("span", """style="font-weight: bold"""") :: Element.start("div") :: Element.end("div") :: Text("Alle Menschen werden Brüder") :: Element.end("span") :: Nil

      val para = transform.convertToPara(spans)

      para.inlineContent must haveLength(1)
      para.inlineContent.head mustEqual InlineContent("Alle Menschen werden Brüder", "font-weight: bold", "", ContentType.Text)
    }

    "ignore text before a span" in {
      val spans =
        Text("Alle Menschen werden Brüder") ::
          Element.start("span", """style="font-weight: bold"""") ::
          Element.start("div") ::
          Element.end("div") ::
          Text("Alle Menschen werden Brüder") ::
          Element.end("span") ::
          Nil

      val para = transform.convertToPara(spans)

      para.inlineContent must haveLength(1)
      para.inlineContent.head mustEqual InlineContent("Alle Menschen werden Brüder", "font-weight: bold", "", ContentType.Text)
    }

    "correctly handle images" in {
      val nodes =
        Element.empty("img", """ src="images/cloud.png" style="margin: 1px" """) ::
        Element.start("span", """style="font-weight: bold"""") ::
        Element.start("div") ::
        Element.end("div") ::
        Text("Alle Menschen werden Brüder") ::
        Element.end("span") ::
        Nil

      val para = transform.convertToPara(nodes)

      para.inlineContent must haveLength(2)
      para.inlineContent.head mustEqual InlineContent("images/cloud.png", "margin: 1px", "", ContentType.Image)
      para.inlineContent(1) mustEqual InlineContent("Alle Menschen werden Brüder", "font-weight: bold", "", ContentType.Text)
    }

    "correctly handle span with <br>" in {
      val l1 = "All hail Osiris, Lord of the Netherworld!"
      val l2 = "All hail Ammit, weigher of hearts!"
      val l3 = "All hail Horus, protector and healer!"
      val nodes =
        Element.start("p") ::
        Element.start("span") ::
        Text(l1) ::
        Element.empty("br") ::
        Text(l2) ::
        Element.empty("br") ::
        Text(l3) ::
        Element.end("span") ::
        Element.end("p") ::
        Nil

      val para = transform.convertToPara(nodes)

      para.inlineContent mustEqual InlineContent.txt(l1) :: InlineContent.br :: InlineContent.txt(l2) :: InlineContent.br :: InlineContent.txt(l3) :: Nil
    }
  }

//  "converting back to HTML" should {
//    "work" in {
//      val chapter = transform.transform(Paths.get("/Users/rhys/bookcompiler/google-html/chapter0.html"))
//      val toXmlTransform = new InternalToHtmlXmlTransform()
//      val xml = toXmlTransform.transform(chapter)
//
//      autoClose(Files.newBufferedWriter(Paths.get("/Users/rhys/bookcompiler/xml/chapter0.html")))(os => os.write(xml.toString()))
//
//      ok
//    }
//  }

}
