package com.rhyssoft.bookcompiler

import java.io.ByteArrayInputStream
import java.nio.charset.StandardCharsets
import java.nio.file.{Files, Paths, StandardCopyOption}
import java.util.stream.Collectors

import com.rhyssoft.IOUtils._
import com.rhyssoft.bookcompiler.model.{ContentType, InlineContent, Paragraph}
import com.rhyssoft.bookcompiler.transforms.{InternalToRhysSoftFormat, RhysSoftFormatToInternalModel}
import com.rhyssoft.rewriter.GoogleHtmlToInternalModel
import org.specs2.mutable.Specification
import org.specs2.specification.core.Fragments

import scala.collection.JavaConverters._
import scala.util.{Failure, Try}

class TransformGoogleToRhysSoftFormatEndToEndTest extends Specification {

  private val rhysSoftFormatToInternalModel = RhysSoftFormatToInternalModel

  "Transforming all Google HTML to internal mode, then to RhysSoft format, then back to internal model" should {
    "yield the same internal model both times" in {

      val gChapters = autoClose(Files.list(Paths.get("/Users/rhys/bookcompiler/google-html/"))){ stream =>
        stream.filter(_.getFileName.toString.endsWith(".html")).collect(Collectors.toList()).asScala
      }.zipWithIndex
       .map{ case (path,i) => GoogleHtmlToInternalModel.convertGoogleHtmlToInternal(ChapterFile(path), i + 1)}

      val chapterPairs = gChapters.map(gChapter => Try{

        // convert to RhysSoft format
        val chapterInRhysSoftFormat = InternalToRhysSoftFormat.process(gChapter)

        val tempFile = Files.createTempFile(s"chapter-${gChapter.metadata.chapterNumber}__", ".rs.txt")
        println(tempFile)
        val rChapter = Try {
          Files.copy(
            new ByteArrayInputStream(chapterInRhysSoftFormat.getBytes(StandardCharsets.UTF_8)),
            tempFile,
            StandardCopyOption.REPLACE_EXISTING
          )
          rhysSoftFormatToInternalModel.process(tempFile)
        }.get
//        Files.delete(tempFile)

        (gChapter, rChapter)
      })

      var chapNum = 0
      Fragments.foreach(chapterPairs){ tryChapters =>
        chapNum = chapNum + 1
        s"chapter $chapNum" ! {

          tryChapters match {
            case Failure(t) => throw t
            case _ =>
          }

          val (ch1, ch2) = tryChapters.get

          for (pNum <- ch1.paragraphs.indices) {
            ch2.paragraphs(pNum) mustEqual updateImages(updateInlineStyles(ch1.paragraphs(pNum)))
          }

          ok
        }

      }
    }
  }

  private def updateInlineStyles(p: Paragraph): Paragraph = {
    p.copy(
      inlineContent = p.inlineContent.map(ic => {
        ic.copy(cssStyle = expectedStyle(ic.cssStyle))
      }),
      cssStyle = ""  // we totally ignore the css style for paragraphs!
    )
  }

  private def updateImages(p: Paragraph): Paragraph = {
    p.copy(
      inlineContent = p.inlineContent.map(ic => {
        if (ic.contentType == ContentType.Image)
          InlineContent("images/bun2.png", "", "", ContentType.Image)
        else ic
      })
    )
  }

  private def expectedStyle(style: String): String = {
    style match {
      case "font-weight:400" => "bold"
      case "font-style:italic" => "italic"
      case "padding:0;margin:0;font-size:21pt;font-family:&quot;Trebuchet MS&quot;;line-height:1.15;page-break-after:avoid;orphans:2;widows:2;text-align:center" => ""
      case "padding-top:0pt;margin:0;color:#d9ead3;padding-left:0;font-size:13pt;padding-bottom:10pt;line-height:1.15;page-break-after:avoid;font-style:italic;font-family:&quot;Trebuchet MS&quot;;orphans:2;widows:2;text-align:center;padding-right:0" => ""
      case "vertical-align:super" => "super"
      case "font-size:24pt" => "fs24pt"
      case "text-decoration:none;font-style:normal;vertical-align:baseline;font-family:&quot;Arial&quot;;font-size:11pt" => "fs11pt"
      case _ => style
    }
  }
}
