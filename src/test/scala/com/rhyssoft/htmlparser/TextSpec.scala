package com.rhyssoft.htmlparser

import org.specs2.mutable.Specification

class TextSpec extends Specification {

  "Text" should {
    "replace named entities" in {
      Text("one&ndash;two").text mustEqual "one–two"
    }

    "replace numbered entities" in {
      Text("one&#39;two").text mustEqual "one'two"
    }

    "replace multiple numbered entities" in {
      Text("one&#39;s, two&#39;s and three&#39;s").text mustEqual "one's, two's and three's"
    }

    "replace multiple hex numbered entities" in {
      Text("one&#x27;s, two&#x27;s and three&#x27;s").text mustEqual "one's, two's and three's"
    }

    "leave failed look-up as-is" in {
      Text("blue&foo;red").text mustEqual "blue&foo;red"
    }
  }
}
