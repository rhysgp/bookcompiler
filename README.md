# RhysSoft Book Compiler

## To Do
1. Rationalise span styles: only keep styles: bold, italic. May need to rationalise bold because some spans all have ```font-weight:400``` 
1. Rationalise p styles: we're only interested in whether the text is indented, and maybe whether it's centred?

